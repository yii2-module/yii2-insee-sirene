<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use yii\BaseYii;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneNafLoader;

if(!\class_exists('Yii'))
{
	class Yii extends BaseYii
	{
		// nothing to add
	}
}

/**
 * InseeSireneNafLoaderTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Module\Yii2InseeSirene\Components\InseeSireneNafLoader
 *
 * @internal
 *
 * @small
 */
class InseeSireneNafLoaderTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var InseeSireneNafLoader
	 */
	protected InseeSireneNafLoader $_object;
	
	public function testPreviousSimple() : void
	{
		$this->assertEquals('00.00', $this->_object->getPrevious('00.01'));
	}
	
	public function testPreviousComplex() : void
	{
		$this->assertEquals('00.0Z', $this->_object->getPrevious('00.10'));
	}
	
	public function testPreviousMoreComplex() : void
	{
		$this->assertEquals('00.ZZ', $this->_object->getPrevious('01.00'));
	}
	
	public function testPreviousMoreMoreComplex() : void
	{
		$this->assertEquals('0Z.ZZ', $this->_object->getPrevious('10.00'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeSireneNafLoader(true);
	}
	
}
