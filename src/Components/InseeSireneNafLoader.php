<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApeInterface;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv5Subclass;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv5Subclass;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv6Artisanat;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv4Class;

/**
 * InseeSireneNafLoader class file.
 * 
 * Cache to avoid database accesses when crawling through naf hierarchy.
 * 
 * @author Anastaszor
 */
class InseeSireneNafLoader
{
	
	/**
	 * The character that is the previous one.
	 * 
	 * @var array<integer|string, string>
	 */
	protected static array $prevchars = [
		'0' => 'Z',
		'1' => '0',
		'2' => '1',
		'3' => '2',
		'4' => '3',
		'5' => '4',
		'6' => '5',
		'7' => '6',
		'8' => '7',
		'9' => '8',
		'A' => '9',
		'B' => 'A',
		'C' => 'B',
		'D' => 'C',
		'E' => 'D',
		'F' => 'E',
		'G' => 'F',
		'H' => 'G',
		'I' => 'H',
		'J' => 'I',
		'K' => 'J',
		'L' => 'K',
		'M' => 'L',
		'N' => 'M',
		'O' => 'N',
		'P' => 'O',
		'Q' => 'P',
		'R' => 'Q',
		'S' => 'R',
		'T' => 'S',
		'U' => 'T',
		'V' => 'U',
		'W' => 'V',
		'X' => 'W',
		'Y' => 'X',
		'Z' => 'Y',
	];
	
	/**
	 * The ids of the nap to the ids of the naf0.
	 * 
	 * @var array<string, string>
	 */
	protected array $_napN4Classes = [];
	
	/**
	 * The ids of the naf0 to the ids of the naf1.
	 * 
	 * @var array<string, string>
	 */
	protected array $_naf1993Lv5Subclasses = [];
	
	/**
	 * The ids of the naf1 to the ids of the naf2.
	 * 
	 * @var array<string, string>
	 */
	protected array $_naf2003Lv5Subclasses = [];
	
	/**
	 * The ids of the naf2.
	 * 
	 * @var array<string, string>
	 */
	protected array $_naf2008Lv5Subclasses = [];
	
	/**
	 * The ids of the naf2 artisanat to the ids of the naf2 subclasses.
	 * 
	 * @var array<string, string>
	 */
	protected array $_naf2008Lv6Artisanat = [];
	
	/**
	 * Builds a new InseeSireneNafLoader with the loading of the naf classes.
	 * 
	 * @param boolean $test whether this object is in phpunit test mode
	 */
	public function __construct(bool $test = false)
	{
		if($test)
		{
			return;
		}
		
		/** @var InseeNap1973Lv4Class $napN4Class */
		foreach(InseeNap1973Lv4Class::find()->all() as $napN4Class)
		{
			$this->_napN4Classes[$napN4Class->insee_nap1973_lv4_class_id] = $napN4Class->insee_naf1993_lv5_subclass_id;
		}
		
		/** @var InseeNaf1993Lv5Subclass $naf0N5Subclass */
		foreach(InseeNaf1993Lv5Subclass::find()->all() as $naf0N5Subclass)
		{
			$this->_naf1993Lv5Subclasses[$naf0N5Subclass->insee_naf1993_lv5_subclass_id] = $naf0N5Subclass->insee_naf2003_lv5_subclass_id;
		}
		
		/** @var InseeNaf2003Lv5Subclass $naf1N5Subclass */
		foreach(InseeNaf2003Lv5Subclass::find()->all() as $naf1N5Subclass)
		{
			$this->_naf2003Lv5Subclasses[$naf1N5Subclass->insee_naf2003_lv5_subclass_id] = $naf1N5Subclass->insee_naf2008_lv5_subclass_id;
		}
		
		/** @var InseeNaf2008Lv5Subclass $naf2N5Subclass */
		foreach(InseeNaf2008Lv5Subclass::find()->all() as $naf2N5Subclass)
		{
			$this->_naf2008Lv5Subclasses[$naf2N5Subclass->insee_naf2008_lv5_subclass_id] = $naf2N5Subclass->insee_naf2008_lv5_subclass_id;
		}
		
		/** @var InseeNaf2008Lv6Artisanat $naf2N6Artisanat */
		foreach(InseeNaf2008Lv6Artisanat::find()->all() as $naf2N6Artisanat)
		{
			$this->_naf2008Lv6Artisanat[$naf2N6Artisanat->insee_naf2008_lv6_artisanat_id] = $naf2N6Artisanat->insee_naf2008_lv5_subclass_id;
		}
	}
	
	/**
	 * Gets a suitable naf2008lv5 subclass id for the given id activite.
	 * 
	 * @param ?string $idActivite
	 * @param ?ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	 * @return ?string
	 * @throws InvalidArgumentException if the nomenclature fails
	 */
	public function getNaf2008Lv5SubclassId(?string $idActivite, ?ApiFrInseeSireneNomenclatureApeInterface $nomenclature) : ?string
	{
		if(null === $idActivite || null === $nomenclature)
		{
			return null;
		}
		
		if('BA.IL' === $idActivite)
		{
			return null;
		}
		
		$ncode = $nomenclature->getCode();
		$codeact = $idActivite;
		
		if('NAFRev2' !== $ncode)
		{
			$codeact = $this->getNaf2003Lv5SubclassId($idActivite, $nomenclature);
		}
		
		$overflow = 1000;
		
		while(!isset($this->_naf2008Lv5Subclasses[$codeact]) && 0 < $overflow--)
		{
			$codeact = $this->getPrevious($codeact);
		}
		
		return $this->_naf2008Lv5Subclasses[$codeact] ?? null;
	}
	
	/**
	 * Gets the code activite from the naf2003 referentiel.
	 *
	 * @param string $idActivite
	 * @param ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	 * @return ?string
	 * @throws InvalidArgumentException
	 */
	public function getNaf2003Lv5SubclassId(string $idActivite, ApiFrInseeSireneNomenclatureApeInterface $nomenclature) : ?string
	{
		$ncode = $nomenclature->getCode();
		$codeact = $idActivite;
		
		if('NAFRev1' !== $ncode)
		{
			$codeact = $this->getNaf1993Lv5SubclassId($idActivite, $nomenclature);
		}
		
		$overflow = 1000;
		
		while(!isset($this->_naf2003Lv5Subclasses[$codeact]) && 0 < $overflow--)
		{
			$codeact = $this->getPrevious($codeact);
		}
		
		return $this->_naf2003Lv5Subclasses[$codeact] ?? null;
	}
	
	/**
	 * Gets the code activite from the naf1993 referentiel.
	 *
	 * @param string $idActivite
	 * @param ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	 * @return ?string
	 * @throws InvalidArgumentException
	 */
	public function getNaf1993Lv5SubclassId(string $idActivite, ApiFrInseeSireneNomenclatureApeInterface $nomenclature) : ?string
	{
		$ncode = $nomenclature->getCode();
		$codeact = $idActivite;
		
		if('NAF1993' !== $ncode)
		{
			$codeact = $this->getNapN4ClassId($idActivite, $nomenclature);
		}
		
		$overflow = 1000;
		
		while(!isset($this->_naf1993Lv5Subclasses[$codeact]) && 0 < $overflow--)
		{
			$codeact = $this->getPrevious($codeact);
		}
		
		return $this->_naf1993Lv5Subclasses[$codeact] ?? null;
	}
	
	/**
	 * Gets the code activite from the nap referentiel.
	 * 
	 * @param string $idActivite
	 * @param ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	 * @return ?string
	 * @throws InvalidArgumentException
	 */
	public function getNapN4ClassId(string $idActivite, ApiFrInseeSireneNomenclatureApeInterface $nomenclature) : ?string
	{
		$ncode = $nomenclature->getCode();
		$codeact = $idActivite;
		
		if('NAP' !== $ncode)
		{
			$message = 'Failed to get the right nomenclature id for {value} {nomenclature}';
			$context = [
				'{value}' => $idActivite,
				'{nomenclature}' => $nomenclature->getCode(),
			];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$overflow = 1000;
		
		while(!isset($this->_napN4Classes[$codeact]) && 0 < $overflow--)
		{
			$codeact = $this->getPrevious($codeact);
		}
		
		return $this->_napN4Classes[$codeact] ?? null;
	}
	
	/**
	 * Gets the previous value of a given code.
	 * 
	 * @param ?string $nafcode
	 * @return string
	 */
	public function getPrevious(?string $nafcode) : string
	{
		$newval = \mb_strtoupper((string) $nafcode);
		
		for($i = \mb_strlen((string) $nafcode, '8bit') - 1; 0 <= $i; $i--)
		{
			$char = $newval[$i];
			if(isset(self::$prevchars[$char]))
			{
				$newval[$i] = self::$prevchars[$char];
				
				if('0' !== $char)
				{
					break;
				}
			}
		}
		
		return $newval;
	}
	
	/**
	 * Gets a suitable id artisanat for the given id artisanat.
	 * 
	 * @param ?string $idArtisanat
	 * @return ?string
	 */
	public function getNaf2008Lv6ArtisanatId(?string $idArtisanat) : ?string
	{
		if(null === $idArtisanat || '' === $idArtisanat)
		{
			return null;
		}
		
		if(isset($this->_naf2008Lv6Artisanat[$idArtisanat]))
		{
			return $idArtisanat;
		}
		
		return null;
	}
	
}
