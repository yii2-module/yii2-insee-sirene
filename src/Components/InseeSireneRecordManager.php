<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use yii\db\ActiveRecord;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN1;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv1Section;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;
use Yii2Module\Yii2Log\Models\Log;

/**
 * InseeSireneRecordManager class file.
 * 
 * This class represents a generic manager for all imported records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeSireneRecordManager extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The naf loader.
	 *
	 * @var InseeSireneNafLoader
	 */
	protected InseeSireneNafLoader $_nafLoader;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
		$this->_nafLoader = new InseeSireneNafLoader();
	}
	
	/**
	 * Keeps all the connections alive by trying to get an object from each
	 * connection.
	 */
	public function keepalive() : void
	{
		InseeSireneMetadata::find()->one();
		InseeBanMetadata::find()->one();
		InseeCatjurN1::find()->one();
		InseeCogMetadata::find()->one();
		InseeNaf1993Lv1Section::find()->one();
		Log::find()->one();
	}
	
	/**
	 * Saves the given record, returns 1 if saved and throws an exception if
	 * it was not saved.
	 *
	 * @param ActiveRecord $record
	 * @return integer
	 * @throws RuntimeException
	 * @todo deprecated
	 */
	public function saveRecord(ActiveRecord $record) : int
	{
		try
		{
			if($record->save())
			{
				return 1;
			}
			
			$errors = [];
			
			foreach($record->getErrorSummary(true) as $error)
			{
				$errors[] = (string) $error;
			}
			
			$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
			$context = [
				'{class}' => \get_class($record),
				'{errs}' => \implode(',', $errors),
				'{obj}' => \json_encode($record->getAttributes(), \JSON_PRETTY_PRINT),
				'{old}' => \json_encode($record->getOldAttributes(), \JSON_PRETTY_PRINT),
			];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		catch(\yii\db\Exception $exc)
		{
			$message = "Failed to save {class} : {msg}\n{obj}\n{old}";
			$context = [
				'{class}' => \get_class($record),
				'{msg}' => $exc->getMessage(),
				'{obj}' => \json_encode($record->getAttributes(), \JSON_PRETTY_PRINT),
				'{old}' => \json_encode($record->getOldAttributes(), \JSON_PRETTY_PRINT),
			];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * Finds the record from its primary key or creates one if not found.
	 *
	 * @template T of ActiveRecord
	 * @param class-string<T> $recordClass
	 * @param array<string, integer|string> $pkValues
	 * @param array<string, null|integer|float|string> $fieldValues
	 * @return T
	 * @throws InvalidArgumentException
	 * @todo deprecated
	 */
	public function findOrCreate(string $recordClass, array $pkValues, array $fieldValues = []) : ActiveRecord
	{
		/** @var ?T $object */
		$object = $recordClass::findOne($pkValues);
		if(null === $object)
		{
			/** @psalm-suppress UnsafeInstantiation */
			$object = new $recordClass();
			
			foreach($pkValues as $pkKey => $pkValue)
			{
				try
				{
					$object->setAttribute($pkKey, $pkValue);
				}
				catch(\yii\base\InvalidArgumentException $exc)
				{
					throw new InvalidArgumentException('Failed to set inexistant attribute '.((string) $pkKey), -1, $exc);
				}
			}
		}
		
		foreach($fieldValues as $fieldKey => $fieldValue)
		{
			if(null !== $fieldValue)
			{
				try
				{
					$object->setAttribute($fieldKey, $fieldValue);
				}
				catch(\yii\base\InvalidArgumentException $exc)
				{
					throw new InvalidArgumentException('Failed to set inexistant attribute '.((string) $fieldKey), -1, $exc);
				}
			}
		}
		
		return $object;
	}
	
	/**
	 * Gets whether the challenger is more recent than the reference (Y-m-d).
	 *
	 * @param ?string $referenceStr
	 * @param DateTimeInterface $challenger
	 * @return boolean
	 * @throws RuntimeException
	 */
	public function isMoreRecentStr(?string $referenceStr, DateTimeInterface $challenger) : bool
	{
		if(null === $referenceStr)
		{
			return true;
		}
		
		$reference = DateTimeImmutable::createFromFormat('Y-m-d', $referenceStr);
		if(false === $reference)
		{
			$message = 'Failed to get datetime value with format Y-m-d from {value}';
			$context = ['{value}' => $referenceStr];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $this->isMoreRecent($reference, $challenger);
	}
	
	/**
	 * Gets whether the challenger is more recent than the reference.
	 *
	 * @param DateTimeInterface $reference
	 * @param DateTimeInterface $challenger
	 * @return boolean
	 */
	public function isMoreRecent(DateTimeInterface $reference, DateTimeInterface $challenger) : bool
	{
		$referencedays = 12 * 32 * ((int) $reference->format('Y')) + 32 * ((int) $reference->format('m') - 1) + ((int) $reference->format('d'));
		$challengerdays = 12 * 32 * ((int) $challenger->format('Y')) + 32 * ((int) $challenger->format('m') - 1) + ((int) $challenger->format('d'));
		
		return $referencedays < $challengerdays;
	}
	
}
