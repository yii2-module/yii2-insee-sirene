<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifsInterface;
use RuntimeException;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneTrancheEffectif;

/**
 * InseeSireneTrancheEffectifUpdater class file.
 * 
 * This class updates the InseeSireneTrancheEffectif records.
 * 
 * @author Anastaszor
 */
class InseeSireneTrancheEffectifUpdater extends InseeSireneRecordManager
{
	
	/**
	 * Updates all the sirene tranche effectifs.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force
	 * @return integer the number of records saved
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Tranche Effectifs');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_tranche_effectif');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$count = 0;
		
		foreach($endpoint->getTrancheEffectifIterator() as $k => $trancheEffectif)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} Tranche Effectifs', ['k' => (int) $k + 1]);
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			$count += $this->updateTrancheEffectif($trancheEffectif);
		}
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_tranche_effectif';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$mdr->save();
		}
		
		return $count;
	}
	
	/**
	 * Updates the records from the given tranche effectifs.
	 * 
	 * @param ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectif
	 * @return integer the number of records saved
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 */
	public function updateTrancheEffectif(ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectif) : int
	{
		$record = InseeSireneTrancheEffectif::findOne($trancheEffectif->getId());
		if(null === $record)
		{
			$record = new InseeSireneTrancheEffectif();
			$record->insee_sirene_tranche_effectif_id = (int) $trancheEffectif->getId();
		}
		$record->code = (string) $trancheEffectif->getCode();
		$record->description = (string) $trancheEffectif->getName();
		$record->min = (int) $trancheEffectif->getEffectifMin();
		$record->max = (int) $trancheEffectif->getEffectifMax();
		if($record->save())
		{
			return 1;
		}
		
		$errors = [];
		
		foreach($record->getErrorSummary(true) as $error)
		{
			$errors[] = (string) $error;
		}
		
		$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
		$context = [
			'{class}' => \get_class($record),
			'{errs}' => \implode(',', $errors),
			'{obj}' => \json_encode($record->getAttributes(), \JSON_PRETTY_PRINT),
			'{old}' => \json_encode($record->getOldAttributes(), \JSON_PRETTY_PRINT),
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
}
