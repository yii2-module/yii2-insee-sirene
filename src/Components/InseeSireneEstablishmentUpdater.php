<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissementInterface;
use RuntimeException;
use Yii2Module\Yii2InseeBan\Components\InseeBanBayesianAddress;
use Yii2Module\Yii2InseeBan\InseeBanModule;
use Yii2Module\Yii2InseeCog\InseeCogModule;
use Yii2Module\Yii2InseeCog\Models\InseeCogPays;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishment;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentAddress;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;

/**
 * InseeSireneEstablishmentUpdater class file.
 * 
 * This class updates the InseeSireneEstablishment and derivated records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @TODO update the history tables when needed, not all of them are historized
 */
class InseeSireneEstablishmentUpdater extends InseeSireneEstablishmentManager
{
	
	public const BATCH_SIZE = 500;
	
	/**
	 * Map of the code communes to code country (fr + domtom).
	 * 
	 * @var array<string, int>
	 */
	protected $_communeCountryMap = [];
	
	/**
	 * Map of the cog code country to db code country (international).
	 * 
	 * @var array<string, int>
	 */
	protected $_codeCountryMap = [];
	
	/**
	 * Map of the libelle country to db code country (international).
	 * 
	 * @var array<string, int>
	 */
	protected $_libelleCountryMap = [];
	
	/**
	 * Gets the most recent datetime from etablissement, or a default one if none.
	 * 
	 * @param ApiFrInseeSireneEtablissementInterface $etablissement
	 * @return DateTimeInterface
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getDateTime(ApiFrInseeSireneEtablissementInterface $etablissement) : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return $etablissement->getDateDernierTraitementEtablissement()
			?? $etablissement->getDateDebut()
			?? $etablissement->getDateCreationEtablissement()
			?? DateTimeImmutable::createFromFormat('Y-m-d', '1900-01-01');
	}
	
	/**
	 * Updates all the sirene establishments.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Establishments');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_establishment');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_sirene_establishment.'.$dbdate->format('Y-m');
		$mdc = InseeSireneMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeSireneMetadata();
			$mdc->insee_sirene_metadata_id = $mdcid;
			$mdc->contents = '0';
			$this->saveRecord($mdc);
		}
		
		$count = 0;
		$batch = [];
		
		foreach($endpoint->getLatestStockEtablissementIterator() as $k => $etablissement)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Establishment Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Establishment Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$this->saveRecord($mdc);
				$this->keepalive();
				
				// we need to empty the batch if we have to stop
				$count += $this->updateEstablishmentBatch($batch);
				$batch = [];
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			$batch[] = $etablissement;
			if(\count($batch) < self::BATCH_SIZE)
			{
				continue;
			}
			
			// regular batch emptying
			$count += $this->updateEstablishmentBatch($batch);
			$batch = [];
		}
		
		// we need to empty the batch if we have to stop
		$count += $this->updateEstablishmentBatch($batch);
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_establishment';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$this->saveRecord($mdr);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment dependancies
	 * by using select batch.
	 * 
	 * @param array<integer, ApiFrInseeSireneEtablissementInterface> $batch
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 */
	public function updateEstablishmentBatch(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		
		/** @var ApiFrInseeSireneEtablissementInterface $etablissementLine */
		foreach($batch as $etablissementLine)
		{
			$pks[] = [
				'insee_sirene_establishment_id' => $etablissementLine->getSiret(),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneEstablishment> $etablissements */
		$etablissements = InseeSireneEstablishment::findAll($pks);
		/** @var array<integer, InseeSireneEstablishmentAddress> $addresses */
		$addresses = InseeSireneEstablishmentAddress::findAll($pks);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, InseeSireneEstablishment> $orderedEstablishments */
		$orderedEstablishments = [];
		/** @var array<integer, InseeSireneEstablishmentAddress> $orderedAddresses */
		$orderedAddresses = [];
		
		foreach($etablissements as $record)
		{
			$orderedEstablishments[(int) $record->insee_sirene_establishment_id] = $record;
		}
		
		foreach($addresses as $record)
		{
			$orderedAddresses[(int) $record->insee_sirene_establishment_id] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $etablissementLine)
		{
			try
			{
				$count += $this->updateEstablishment(
					$etablissementLine,
					$orderedEstablishments[(int) $etablissementLine->getSiret()] ?? null,
					$orderedAddresses[(int) $etablissementLine->getSiret()] ?? null,
				);
			}
			catch(RuntimeException $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment.
	 *
	 * @param ApiFrInseeSireneEtablissementInterface $apiEtablissement
	 * @param ?InseeSireneEstablishment $establishment
	 * @param ?InseeSireneEstablishmentAddress $address
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function updateEstablishment(
		ApiFrInseeSireneEtablissementInterface $apiEtablissement,
		?InseeSireneEstablishment $establishment,
		?InseeSireneEstablishmentAddress $address,
		bool $force = false
	) : int {
		
		$mustBeSaved = false;
		
		if(null === $establishment)
		{
			$establishment = new InseeSireneEstablishment();
			$establishment->insee_sirene_establishment_id = (int) $apiEtablissement->getSiret();
			$establishment->insee_sirene_legal_unit_id = (int) $apiEtablissement->getSiren();
			$mustBeSaved = true;
		}
		
		$ddt = $apiEtablissement->getDateDernierTraitementEtablissement();
		if(null !== $ddt)
		{
			if(!$force && null !== $establishment->date_last_treatment && '' !== $establishment->date_last_treatment)
			{
				$dlt = DateTimeImmutable::createFromFormat('Y-m-d', $establishment->date_last_treatment);
				if(false !== $dlt)
				{
					$dltdays = 12 * 32 * ((int) $dlt->format('Y')) + 32 * ((int) $dlt->format('m') - 1) + ((int) $dlt->format('d'));
					$ddtdays = 12 * 32 * ((int) $ddt->format('Y')) + 32 * ((int) $ddt->format('m') - 1) + ((int) $ddt->format('d'));
					if($dltdays >= $ddtdays)
					{
						return 0;
					}
				}
			}
			
			// after comparison for what exists
			$establishment->date_last_treatment = $ddt->format('Y-m-d');
		}
		
		$dateCreation = $apiEtablissement->getDateCreationEtablissement();
		if(null !== $dateCreation && (string) $establishment->date_creation !== $dateCreation->format('Y-m-d'))
		{
			$establishment->date_creation = $dateCreation->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		try
		{
			$cogPaysId = $this->getCogPaysId(
				$apiEtablissement->getCodeCommuneEtablissement(),
				$apiEtablissement->getLibelleCommuneEtablissement(),
				$apiEtablissement->getCodePaysEtrangerEtablissement(),
				$apiEtablissement->getLibellePaysEtrangerEtablissement(),
			);
		}
		catch(RuntimeException $exc)
		{
			$msg = '';
			
			do
			{
				$msg .= $exc->getMessage()."\n\n".$exc->getTraceAsString()."\n\n\n\n";
				$exc = $exc->getPrevious();
			}
			while(null !== $exc);
			
			$this->_logger->warning($msg);
			$cogPaysId = 99100; // metropolitan france
		}
		
		if($establishment->insee_cog_pays_id !== $cogPaysId)
		{
			$establishment->insee_cog_pays_id = $cogPaysId;
			$mustBeSaved = true;
		}
		
		$dateSince = $this->getDateTime($apiEtablissement);
		
		$primaryComplement = $apiEtablissement->getComplementAdresseEtablissement();
		if(null !== $primaryComplement && '' !== $primaryComplement && $establishment->complement_address !== $primaryComplement)
		{
			$establishment->complement_address = $primaryComplement;
			$mustBeSaved = true;
		}
		
		$primaryForeignCity = $apiEtablissement->getLibelleCommuneEtrangerEtablissement();
		if(null !== $primaryForeignCity && '' !== $primaryForeignCity && $establishment->libelle_foreign_city !== $primaryForeignCity)
		{
			$establishment->libelle_foreign_city = $primaryForeignCity;
			$mustBeSaved = true;
		}
		
		$primarySpecialDistri = $apiEtablissement->getDistributionSpecialeEtablissement();
		if(null !== $primarySpecialDistri && '' !== $primarySpecialDistri && $establishment->special_distribution !== $primarySpecialDistri)
		{
			$establishment->special_distribution = $primarySpecialDistri;
			$mustBeSaved = true;
		}
		
		$primaryCedexCode = $apiEtablissement->getCodeCedexEtablissement();
		if(null !== $primaryCedexCode && '' !== $primaryCedexCode && $establishment->cedex_code !== $primaryCedexCode)
		{
			$establishment->cedex_code = $primaryCedexCode;
			$mustBeSaved = true;
		}
		
		$primaryCedexLabel = $apiEtablissement->getLibelleCedexEtablissement();
		if(null !== $primaryCedexLabel && '' !== $primaryCedexLabel && $establishment->cedex_label !== $primaryCedexLabel)
		{
			$establishment->cedex_label = $primaryCedexLabel;
			$mustBeSaved = true;
		}
		
		$fullAddress = ((string) $apiEtablissement->getNumeroVoieEtablissement()).((string) $apiEtablissement->getIndiceRepetitionEtablissement());
		$typeVoie = $apiEtablissement->getTypeVoieEtablissement();
		if(null !== $typeVoie)
		{
			$fullAddress .= ' '.((string) $typeVoie->getName());
		}
		$fullAddress .= ' '.((string) $apiEtablissement->getLibelleVoieEtablissement());
		$fullAddress .= "\n".((string) $apiEtablissement->getComplementAdresseEtablissement());
		$fullAddress .= "\n".((string) $apiEtablissement->getCodePostalEtablissement()).' '.((string) $apiEtablissement->getLibelleCommuneEtablissement());
		$fullAddress = \trim($fullAddress);
		if($establishment->insee_ban_address_raw !== $fullAddress)
		{
			$establishment->insee_ban_address_raw = $fullAddress;
			$mustBeSaved = true;
		}
		
		if(null !== $establishment->insee_ban_address_raw && '' !== $establishment->insee_ban_address_raw
			&& null === $establishment->insee_ban_address_id && empty($establishment->insee_ban_address_fitness) /* || (int) $record->insee_ban_address_fitness < 230 */)
		{
			try
			{
				$primaryBanAddress = $this->getBanAddress(
					$apiEtablissement->getNumeroVoieEtablissement(),
					$apiEtablissement->getIndiceRepetitionEtablissement(),
					$apiEtablissement->getTypeVoieEtablissement(),
					$apiEtablissement->getLibelleVoieEtablissement(),
					$apiEtablissement->getComplementAdresseEtablissement(),
					$apiEtablissement->getCodePostalEtablissement(),
					$apiEtablissement->getLibelleCommuneEtablissement(),
					$apiEtablissement->getCodeCommuneEtablissement(),
				);
				
				if(null !== $primaryBanAddress)
				{
					$establishment->insee_ban_address_id = $primaryBanAddress->getAddress()->insee_ban_address_id;
					$establishment->insee_ban_address_fitness = (int) (255.0 * $primaryBanAddress->getFitness());
					$mustBeSaved = true;
				}
			}
			catch(RuntimeException $exc)
			{
				$msg = '';
				
				do
				{
					$msg .= $exc->getMessage()."\n\n".$exc->getTraceAsString()."\n\n\n\n";
					$exc = $exc->getPrevious();
				}
				while(null !== $exc);
				
				$this->_logger->warning($msg);
			}
		}
		if(empty($establishment->insee_ban_address_fitness))
		{
			$establishment->insee_ban_address_fitness = 1;
			$mustBeSaved = true;
		}
		
		$denomination = $apiEtablissement->getDenominationUsuelleEtablissement();
		if(null !== $denomination && '' !== $denomination)
		{
			if($this->isMoreRecentStr($establishment->date_since_usage_denomination, $dateSince))
			{
				if($establishment->usage_denomination !== $denomination)
				{
					$establishment->usage_denomination = $denomination;
					$establishment->date_since_usage_denomination = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$caractereEmployeur = $apiEtablissement->hasCaractereEmployeurEtablissement();
		if(null !== $caractereEmployeur)
		{
			if($this->isMoreRecentStr($establishment->date_since_is_employer, $dateSince))
			{
				if((int) $caractereEmployeur !== $establishment->is_employer)
				{
					$establishment->is_employer = (int) $caractereEmployeur;
					$establishment->date_since_is_employer = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$enseigneDateMustBeUpdated = false;
		$enseigne1 = $apiEtablissement->getEnseigne1Etablissement();
		if(null !== $enseigne1 && '' !== $enseigne1)
		{
			if($this->isMoreRecentStr($establishment->date_since_enseigne, $dateSince))
			{
				if($establishment->enseigne_1 !== $enseigne1)
				{
					$establishment->enseigne_1 = $enseigne1;
					$enseigneDateMustBeUpdated = true;
				}
			}
		}
		
		$enseigne2 = $apiEtablissement->getEnseigne2Etablissement();
		if(null !== $enseigne2 && '' !== $enseigne2)
		{
			if($this->isMoreRecentStr($establishment->date_since_enseigne, $dateSince))
			{
				if($establishment->enseigne_2 !== $enseigne2)
				{
					$establishment->enseigne_2 = $enseigne2;
					$enseigneDateMustBeUpdated = true;
				}
			}
		}
		
		$enseigne3 = $apiEtablissement->getEnseigne3Etablissement();
		if(null !== $enseigne3 && '' !== $enseigne3)
		{
			if($this->isMoreRecentStr($establishment->date_since_enseigne, $dateSince))
			{
				if($establishment->enseigne_3 !== $enseigne3)
				{
					$establishment->enseigne_3 = $enseigne3;
					$enseigneDateMustBeUpdated = true;
				}
			}
		}
		
		if($enseigneDateMustBeUpdated)
		{
			$establishment->date_since_enseigne = $dateSince->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$etatAdministratif = $apiEtablissement->getEtatAdministratifEtablissement();
		if($this->isMoreRecentStr($establishment->date_since_etat_administratif, $dateSince))
		{
			if((int) $etatAdministratif->getId() !== (int) $establishment->insee_sirene_etat_administratif_id)
			{
				$establishment->insee_sirene_etat_administratif_id = (int) $etatAdministratif->getId();
				$establishment->date_since_etat_administratif = $dateSince->format('Y-m-d');
				$mustBeSaved = true;
			}
		}
		
		$activitePrincipale = $apiEtablissement->getActivitePrincipaleEtablissement();
		$nomenclature = $apiEtablissement->getNomenclatureActivitePrincipaleEtablissement();
		if(null !== $activitePrincipale && '' !== $activitePrincipale)
		{
			if($this->isMoreRecentStr($establishment->date_since_naf2008_lv5_subclass, $dateSince))
			{
				try
				{
					$activiteId = $this->_nafLoader->getNaf2008Lv5SubclassId($activitePrincipale, $nomenclature);
				}
				catch(InvalidArgumentException $exc)
				{
					$message = 'Failed to get naf2008 subclass with id {aid} and nomenclature {nom} : {exc}';
					$context = ['aid' => $activitePrincipale, 'nom' => $nomenclature, 'exc' => $exc->getMessage()];
					$this->_logger->warning($message, $context);
					$activiteId = null;
				}
				
				if(null !== $activiteId && (string) $establishment->insee_naf2008_lv5_subclass_id !== (string) $activiteId)
				{
					$establishment->insee_naf2008_lv5_subclass_id = (string) $activiteId;
					$establishment->date_since_naf2008_lv5_subclass = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$artisanat = $apiEtablissement->getActivitePrincipaleRegistreMetiersEtablissement();
		if(null !== $artisanat && '' !== $artisanat)
		{
			if($this->isMoreRecentStr($establishment->date_since_naf2008_lv5_subclass, $dateSince))
			{
				$artisanatId = $this->_nafLoader->getNaf2008Lv6ArtisanatId($artisanat);
				if(null !== $artisanatId && (string) $establishment->insee_naf2008_lv6_artisanat_id !== (string) $artisanatId)
				{
					$establishment->insee_naf2008_lv6_artisanat_id = (string) $artisanatId;
					$establishment->date_since_naf2008_lv6_artisanat = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$siege = $apiEtablissement->hasEtablissementSiege();
		if($this->isMoreRecentStr($establishment->date_since_is_siege, $dateSince))
		{
			if((int) $siege !== $establishment->is_siege)
			{
				$establishment->is_siege = (int) $siege;
				$establishment->date_since_is_siege = $dateSince->format('Y-m-d');
				$mustBeSaved = true;
			}
		}
		
		$trancheEffectif = $apiEtablissement->getTrancheEffectifsEtablissement();
		if(null !== $trancheEffectif)
		{
			if($this->isMoreRecentStr($establishment->date_since_tranche_effectif, $dateSince))
			{
				if($trancheEffectif->getId() !== $establishment->insee_sirene_tranche_effectif_id)
				{
					$establishment->insee_sirene_tranche_effectif_id = (int) $trancheEffectif->getId();
					$establishment->date_since_tranche_effectif = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$count = (int) ($mustBeSaved && (bool) $this->saveRecord($establishment));
		$count += $this->handleSecondaryAddress($establishment, $apiEtablissement, $address);
		
		return $count;
	}
	
	/**
	 * Saves the secondary address of the establishment, if any.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param ApiFrInseeSireneEtablissementInterface $apiEtablissement
	 * @param ?InseeSireneEstablishmentAddress $address
	 * @return integer
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function handleSecondaryAddress(
		InseeSireneEstablishment $establishment,
		ApiFrInseeSireneEtablissementInterface $apiEtablissement,
		?InseeSireneEstablishmentAddress $address
	) : int {
		
		$mustBeSaved = false;
		
		if(null === $address)
		{
			$address = new InseeSireneEstablishmentAddress();
			$address->insee_sirene_establishment_id = $establishment->insee_sirene_establishment_id;
			$mustBeSaved = true;
		}
		
		$fullAddress = ((string) $apiEtablissement->getNumeroVoie2Etablissement()).((string) $apiEtablissement->getIndiceRepetition2Etablissement());
		$typeVoie2 = $apiEtablissement->getTypeVoie2Etablissement();
		if(null !== $typeVoie2)
		{
			$fullAddress .= ' '.((string) $typeVoie2->getName());
		}
		$fullAddress .= ' '.((string) $apiEtablissement->getLibelleVoie2Etablissement());
		$fullAddress .= "\n".((string) $apiEtablissement->getComplementAdresse2Etablissement());
		$fullAddress .= "\n".((string) $apiEtablissement->getCodePostal2Etablissement()).' '.((string) $apiEtablissement->getLibelleCommune2Etablissement());
		$fullAddress = \trim($fullAddress);
		$communeEtranger = $apiEtablissement->getLibelleCommuneEtranger2Etablissement();
		if(null !== $address->insee_ban_address_raw && '' !== $address->insee_ban_address_raw && (null === $communeEtranger || '' === $communeEtranger))
		{
			return 0;
		}
		
		if($address->insee_ban_address_raw !== $fullAddress)
		{
			$address->insee_ban_address_raw = $fullAddress;
			$mustBeSaved = true;
		}
		
		$secondaryComplement = $apiEtablissement->getComplementAdresse2Etablissement();
		if(null !== $secondaryComplement && '' !== $secondaryComplement && $address->complement_address !== $secondaryComplement)
		{
			$address->complement_address = $secondaryComplement;
			$mustBeSaved = true;
		}
		
		$secondaryForeignCity = $apiEtablissement->getLibelleCommuneEtranger2Etablissement();
		if(null !== $secondaryForeignCity && '' !== $secondaryForeignCity && $address->libelle_foreign_city !== $secondaryForeignCity)
		{
			$address->libelle_foreign_city = $secondaryForeignCity;
			$mustBeSaved = true;
		}
		
		$secondarySpecialDistri = $apiEtablissement->getDistributionSpeciale2Etablissement();
		if(null !== $secondarySpecialDistri && '' !== $secondarySpecialDistri && $address->special_distribution !== $secondarySpecialDistri)
		{
			$address->special_distribution = $secondarySpecialDistri;
			$mustBeSaved = true;
		}
		
		$secondaryCedexCode = $apiEtablissement->getCodeCedex2Etablissement();
		if(null !== $secondaryCedexCode && '' !== $secondaryCedexCode && $address->cedex_code !== $secondaryCedexCode)
		{
			$address->cedex_code = $secondaryCedexCode;
			$mustBeSaved = true;
		}
		
		$secondaryCedexLabel = $apiEtablissement->getLibelleCedex2Etablissement();
		if(null !== $secondaryCedexLabel && '' !== $secondaryCedexLabel && $address->cedex_label !== $secondaryCedexLabel)
		{
			$address->cedex_label = $secondaryCedexLabel;
			$mustBeSaved = true;
		}
		
		if((null === $address->insee_ban_address_raw || '' === $address->insee_ban_address_raw)
			&& null === $address->insee_ban_address_id && empty($address->insee_ban_address_fitness)
			/* || (int) $record->insee_ban_address_fitness < 230 */) {
			try
			{
				$secondaryBanAddress = $this->getBanAddress(
					$apiEtablissement->getNumeroVoie2Etablissement(),
					$apiEtablissement->getIndiceRepetition2Etablissement(),
					$apiEtablissement->getTypeVoie2Etablissement(),
					$apiEtablissement->getLibelleVoie2Etablissement(),
					$apiEtablissement->getComplementAdresse2Etablissement(),
					$apiEtablissement->getCodePostal2Etablissement(),
					$apiEtablissement->getLibelleCommune2Etablissement(),
					$apiEtablissement->getCodeCommune2Etablissement(),
				);
				
				if(null !== $secondaryBanAddress)
				{
					$address->insee_ban_address_id = $secondaryBanAddress->getAddress()->insee_ban_address_id;
					$address->insee_ban_address_fitness = (int) (255.0 * $secondaryBanAddress->getFitness());
					$mustBeSaved = true;
				}
			}
			catch(RuntimeException $exc)
			{
				$msg = '';
				
				do
				{
					$msg .= $exc->getMessage()."\n\n".$exc->getTraceAsString()."\n\n\n\n";
					$exc = $exc->getPrevious();
				}
				while(null !== $exc);
				
				$this->_logger->warning($msg);
			}
		}
		if(empty($address->insee_ban_address_fitness))
		{
			$address->insee_ban_address_fitness = 1;
			$mustBeSaved = true;
		}
		
		
		return (int) ($mustBeSaved && (bool) $this->saveRecord($address));
	}
	
	/**
	 * Gets the libelle of the country from the code commune and code foreign
	 * country, if needed and not found in france.
	 * 
	 * @param ?string $codeCommune
	 * @param ?string $libelleCommune
	 * @param ?string $codePaysEtranger
	 * @param ?string $libellePaysEtranger
	 * @return int
	 * @throws RuntimeException if the country cannot be found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getCogPaysId(?string $codeCommune, ?string $libelleCommune, ?string $codePaysEtranger, ?string $libellePaysEtranger) : int
	{
		if(null !== $codeCommune && '' !== $codeCommune)
		{
			if(isset($this->_communeCountryMap[$codeCommune]))
			{
				return $this->_communeCountryMap[$codeCommune];
			}
			
			/** @var InseeCogModule $cogModule */
			$cogModule = InseeCogModule::getInstance();
			
			try
			{
				$cogCommune = $cogModule->getCogCommune($codeCommune, $libelleCommune, (string) \mb_substr($codeCommune, 0, 2, 'UTF-8'));
			}
			catch(RuntimeException|InvalidArgumentException $exc)
			{
				$message = 'Failed to find commune from code commune {code}';
				$context = ['{code}' => $codeCommune];
				
				throw new RuntimeException(\strtr($message, $context), -1, $exc);
			}
			
			$cogDepartement = $cogCommune->inseeCogDepartement;
			$cogRegion = $cogDepartement->inseeCogRegion;
			
			return $this->_communeCountryMap[$codeCommune] = $cogRegion->insee_cog_pays_id;
		}
		
		if(null !== $codePaysEtranger && '' !== $codePaysEtranger)
		{
			if(isset($this->_codeCountryMap[$codePaysEtranger]))
			{
				return $this->_codeCountryMap[$codePaysEtranger];
			}
			
			$cogPays = InseeCogPays::findOne([
				'insee_cog_pays_cog_id' => $codePaysEtranger,
			]);
			if(null === $cogPays)
			{
				$cogPays = InseeCogPays::findOne([
					'insee_cog_pays_id' => $codePaysEtranger.'01',
				]);
				if(null === $cogPays)
				{
					$message = 'Failed to find country from cog country {code}';
					$context = ['{code}' => $codePaysEtranger];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
			
			return $this->_codeCountryMap[$codePaysEtranger] = $cogPays->insee_cog_pays_id;
		}
		
		if(null !== $libellePaysEtranger && '' !== $libellePaysEtranger)
		{
			if(isset($this->_libelleCountryMap[$libellePaysEtranger]))
			{
				return $this->_libelleCountryMap[$libellePaysEtranger];
			}
			
			$cogPays = InseeCogPays::findOne([
				'libelle_simple' => $libellePaysEtranger,
			]);
			if(null === $cogPays)
			{
				$message = 'Failed to find country from libelle {libelle}';
				$context = ['{code}' => $libellePaysEtranger];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			return $this->_libelleCountryMap[$libellePaysEtranger] = $cogPays->insee_cog_pays_id;
		}
		
		// assume it's in metropolitan france
		return 99100;
	}
	
	/**
	 * Gets the ban address id for the given address.
	 * 
	 * @param ?integer $numeroVoie
	 * @param ?string $indiceRepetition
	 * @param ?ApiFrInseeBanTypeVoieInterface $typeVoie
	 * @param ?string $libelleVoie
	 * @param ?string $complementAdresse
	 * @param ?string $codePostal
	 * @param ?string $libelleCommune
	 * @param ?string $codeCommune
	 * @return ?InseeBanBayesianAddress
	 * @throws RuntimeException
	 */
	public function getBanAddress(
		?int $numeroVoie,
		?string $indiceRepetition,
		?ApiFrInseeBanTypeVoieInterface $typeVoie,
		?string $libelleVoie,
		?string $complementAdresse,
		?string $codePostal,
		?string $libelleCommune,
		?string $codeCommune
	) : ?InseeBanBayesianAddress {
		
		if(null !== $typeVoie)
		{
			$libelleVoie = ((string) $typeVoie->getName()).' '.((string) $libelleVoie);
		}
		
		try
		{
			$module = InseeBanModule::getInstance();
			
			$banAddressCollection = $module->getBayesianObjectFinder()->getBanAddress(
				$numeroVoie,
				$indiceRepetition,
				$libelleVoie,
				$complementAdresse,
				$codePostal,
				$libelleCommune,
				$codeCommune,
			);
			
			return $banAddressCollection->getBestFit();
		}
		catch(InvalidArgumentException $exc)
		{
			return null;
		}
	}
	
}
