<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneSuccession;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneSuccessionInterface;
use RuntimeException;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishment;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneSuccession;

/**
 * InseeSireneSuccessionUpdater class file.
 * 
 * This class updates the InseeSireneSuccession records.
 * 
 * @author Anastaszor
 */
class InseeSireneSuccessionUpdater extends InseeSireneRecordManager
{
	
	public const BATCH_SIZE = 500;
	
	/**
	 * Updates all the sirene establishment dependancies.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the number of records updated
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Establishment Dependancies');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_succession');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_sirene_succession.'.$dbdate->format('Y-m');
		$mdc = InseeSireneMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeSireneMetadata();
			$mdc->insee_sirene_metadata_id = $mdcid;
			$mdc->contents = '0';
			$this->saveRecord($mdc);
		}
		
		$count = 0;
		$batch = [];
		
		foreach($endpoint->getLatestStockSuccessionIterator() as $k => $etablissementLien)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Establishment Succession Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Establishment Succession Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$this->saveRecord($mdc);
				$this->keepalive();
				
				// we need to empty the batch if we have to stop
				$count += $this->updateSuccessionBatch($batch);
				$batch = [];
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			$batch[] = $etablissementLien;
			if(\count($batch) < self::BATCH_SIZE)
			{
				continue;
			}
			
			// regular batch emptying
			$count += $this->updateSuccessionBatch($batch);
			$batch = [];
		}
		
		// we need to empty the remaining data if the number of them is not round
		$count += $this->updateSuccessionBatch($batch);
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_succession';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$this->saveRecord($mdr);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment dependancies
	 * by using select batch.
	 * 
	 * @param array<integer, ApiFrInseeSireneSuccessionInterface> $batch
	 * @return integer the number of records updated
	 */
	public function updateSuccessionBatch(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		
		/** @var ApiFrInseeSireneSuccession $successionLine */
		foreach($batch as $successionLine)
		{
			$pks[] = [
				'insee_sirene_establishment_prev_id' => (int) $successionLine->getSiretEtablissementPredecesseur(),
				'insee_sirene_establishment_next_id' => (int) $successionLine->getSiretEtablissementSuccesseur(),
				'date_dependancy' => $successionLine->getDateLienSuccession()->format('Y-m-d'),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneSuccession> $records */
		$records = InseeSireneSuccession::findAll($pks);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, array<integer, array<string, InseeSireneSuccession>>> $orderedRecords */
		$orderedRecords = [];
		
		foreach($records as $record)
		{
			$orderedRecords[(int) $record->insee_sirene_establishment_prev_id][(int) $record->insee_sirene_establishment_next_id][(string) $record->date_dependancy] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $successionLine)
		{
			try
			{
				$count += $this->updateSuccession(
					$successionLine,
					$orderedRecords[(int) $successionLine->getSiretEtablissementPredecesseur()][(int) $successionLine->getSiretEtablissementSuccesseur()][(string) $successionLine->getDateLienSuccession()->format('Y-m-d')] ?? null,
				);
			}
			catch(RuntimeException $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment dependancies.
	 *
	 * @param ApiFrInseeSireneSuccessionInterface $successionLine
	 * @param ?InseeSireneSuccession $record
	 * @return integer the number of records updated
	 * @throws RuntimeException
	 */
	public function updateSuccession(ApiFrInseeSireneSuccessionInterface $successionLine, ?InseeSireneSuccession $record) : int
	{
		$mustBeSaved = false;
		
		if(null === $record)
		{
			$prev = InseeSireneEstablishment::findOne((int) $successionLine->getSiretEtablissementPredecesseur());
			if(null === $prev)
			{
				return 0;
			}
			
			$next = InseeSireneEstablishment::findOne((int) $successionLine->getSiretEtablissementSuccesseur());
			if(null === $next)
			{
				return 0;
			}
			
			$record = new InseeSireneSuccession();
			$record->insee_sirene_establishment_prev_id = $prev->insee_sirene_establishment_id;
			$record->insee_sirene_establishment_next_id = $next->insee_sirene_establishment_id;
			$record->date_dependancy = $successionLine->getDateLienSuccession()->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$newDateLastTreatment = $successionLine->getDateDernierTraitementLienSuccession()->format('Y-m-d');
		if((string) $record->date_last_treatment !== $newDateLastTreatment)
		{
			$record->date_last_treatment = $newDateLastTreatment;
			$mustBeSaved = true;
		}
		
		$newSiegeTransfer = (int) $successionLine->hasTransfertSiege();
		if((int) $record->siege_transfer !== $newSiegeTransfer)
		{
			$record->siege_transfer = $newSiegeTransfer;
			$mustBeSaved = true;
		}
		
		$newEconomicContinuity = (int) $successionLine->hasContinuiteEconomique();
		if((int) $record->economic_continuity !== $newEconomicContinuity)
		{
			$record->economic_continuity = $newEconomicContinuity;
			$mustBeSaved = true;
		}
		
		return (int) ($mustBeSaved && (bool) $this->saveRecord($record));
	}
	
}
