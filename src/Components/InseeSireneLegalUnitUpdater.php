<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegaleInterface;
use RuntimeException;
use Throwable;
use yii\db\ActiveRecord;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnit;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryCategorieEntreprise;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryCatjurN3;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryDenomination;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryEmployer;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistorySocialEconomy;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistorySocieteMission;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryTrancheEffectif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPerson;
use Yii2Module\Yii2InseeSirene\Models\InseeSirenePhysicalPerson;

/**
 * InseeSireneLegalUnitUpdater class file.
 * 
 * This class updates the InseeSireneLegalUnit and derivated records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 */
class InseeSireneLegalUnitUpdater extends InseeSireneLegalUnitManager
{
	
	public const BATCH_SIZE = 500;
	
	/**
	 * Gets the most recent datetime from unite legale, or a default one if none.
	 * 
	 * @param ApiFrInseeSireneUniteLegaleInterface $uniteLegale
	 * @return DateTimeInterface
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getDateSince(ApiFrInseeSireneUniteLegaleInterface $uniteLegale) : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return $uniteLegale->getDateDernierTraitementUniteLegale()
			?? $uniteLegale->getDateDebut()
			?? $uniteLegale->getDateCreationUniteLegale()
			?? DateTimeImmutable::createFromFormat('Y-m-d', '1900-01-01');
	}
	
	/**
	 * Updates all the sirene legal units.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the number of records updated
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Legal Units');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_legal_unit');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_sirene_legal_unit.'.$dbdate->format('Y-m');
		$mdc = InseeSireneMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeSireneMetadata();
			$mdc->insee_sirene_metadata_id = $mdcid;
			$mdc->contents = '0';
			$this->saveRecord($mdc);
		}
		
		$count = 0;
		$batch = [];
		
		foreach($endpoint->getLatestStockUniteLegaleIterator() as $k => $legalUnit)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Legal Unit Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Legal Unit Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$this->saveRecord($mdc);
				$this->keepalive();
				
				// we need to empty the batch if we have to stop
				$count += $this->updateLegalUnitBatch($batch);
				$batch = [];
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			$batch[] = $legalUnit;
			if(\count($batch) < self::BATCH_SIZE)
			{
				continue;
			}
			
			// regular batch emptying
			$count += $this->updateLegalUnitBatch($batch);
			$batch = [];
		}
		
		// we need to empty the batch if we have to stop
		$count += $this->updateLegalUnitBatch($batch);
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_legal_unit';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$this->saveRecord($mdr);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene legal unit dependancies
	 * by using select batch.
	 * 
	 * @param array<integer, ApiFrInseeSireneUniteLegaleInterface> $batch
	 * @return integer the number of records updated
	 */
	public function updateLegalUnitBatch(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		
		/** @var ApiFrInseeSireneUniteLegaleInterface $uniteLegaleLine */
		foreach($batch as $uniteLegaleLine)
		{
			$pks[] = [
				'insee_sirene_legal_unit_id' => $uniteLegaleLine->getSiren(),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneLegalUnit> $legalUnits */
		$legalUnits = InseeSireneLegalUnit::findAll($pks);
		/** @var array<integer, InseeSireneMoralPerson> $moralPersons */
		$moralPersons = InseeSireneMoralPerson::findAll($pks);
		/** @var array<integer, InseeSirenePhysicalPerson> $physicalPersons */
		$physicalPersons = InseeSirenePhysicalPerson::findAll($pks);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, InseeSireneLegalUnit> $orderedLegalUnits */
		$orderedLegalUnits = [];
		/** @var array<integer, InseeSireneMoralPerson> $orderedMoralPersons */
		$orderedMoralPersons = [];
		/** @var array<integer, InseeSirenePhysicalPerson> $orderedPhysicalPersons */
		$orderedPhysicalPersons = [];
		
		foreach($legalUnits as $record)
		{
			$orderedLegalUnits[(int) $record->insee_sirene_legal_unit_id] = $record;
		}
		
		foreach($moralPersons as $record)
		{
			$orderedMoralPersons[(int) $record->insee_sirene_legal_unit_id] = $record;
		}
		
		foreach($physicalPersons as $record)
		{
			$orderedPhysicalPersons[(int) $record->insee_sirene_legal_unit_id] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $uniteLegaleLine)
		{
			try
			{
				$count += $this->updateLegalUnit(
					$uniteLegaleLine,
					$orderedLegalUnits[(int) $uniteLegaleLine->getSiren()] ?? null,
					$orderedMoralPersons[(int) $uniteLegaleLine->getSiren()] ?? null,
					$orderedPhysicalPersons[(int) $uniteLegaleLine->getSiren()] ?? null,
				);
			}
			catch(Throwable $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene legal unit.
	 *
	 * @param ApiFrInseeSireneUniteLegaleInterface $uniteLegale
	 * @param ?InseeSireneLegalUnit $legalUnit
	 * @param ?InseeSireneMoralPerson $moralPerson
	 * @param ?InseeSirenePhysicalPerson $physicalPerson
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function updateLegalUnit(
		ApiFrInseeSireneUniteLegaleInterface $uniteLegale,
		?InseeSireneLegalUnit $legalUnit = null,
		?InseeSireneMoralPerson $moralPerson = null,
		?InseeSirenePhysicalPerson $physicalPerson = null,
		bool $force = false
	) : int {
		
		$mustBeSaved = false;
		/** @var array<integer, ActiveRecord> $recordsToBeSaved */
		$recordsToBeSaved = [];
		
		if(null === $legalUnit)
		{
			$legalUnit = new InseeSireneLegalUnit();
			$legalUnit->insee_sirene_legal_unit_id = (int) $uniteLegale->getSiren();
		}
		
		$ddt = $uniteLegale->getDateDernierTraitementUniteLegale();
		if(null !== $ddt)
		{
			if(!$force && !$this->isMoreRecentStr($legalUnit->date_last_treatment, $ddt))
			{
				return 0;
			}
			
			// after comparison for what exists
			$legalUnit->date_last_treatment = $ddt->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$dateSince = $this->getDateSince($uniteLegale);
		
		$dateCreation = $uniteLegale->getDateCreationUniteLegale();
		if(null !== $dateCreation && (string) $legalUnit->date_creation !== $dateCreation->format('Y-m-d'))
		{
			$legalUnit->date_creation = $dateCreation->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$identifiantAsso = $uniteLegale->getIdentifiantAssociationUniteLegale();
		if(null !== $identifiantAsso && '' !== $identifiantAsso && 'NÉANT' !== $identifiantAsso && (string) $legalUnit->insee_rna_id !== $identifiantAsso)
		{
			$legalUnit->insee_rna_id = $identifiantAsso;
			$mustBeSaved = true;
		}
		
		$categorieEntreprise = $uniteLegale->getCategorieEntreprise();
		if(null !== $categorieEntreprise)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_category_entreprise, $dateSince))
			{
				if(null === $legalUnit->insee_sirene_category_entreprise_id || (int) $legalUnit->insee_sirene_category_entreprise_id !== (int) $categorieEntreprise->getId())
				{
					$recordsToBeSaved[] = $this->handleHistoryInteger(InseeSireneLegalUnitHistoryCategorieEntreprise::class, $legalUnit, (string) $legalUnit->date_since_category_entreprise, 'insee_sirene_category_entreprise_id');
					$legalUnit->insee_sirene_category_entreprise_id = (int) $categorieEntreprise->getId();
					$legalUnit->date_since_category_entreprise = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$categorieJuridique = $this->getCatjurId($uniteLegale->getCategorieJuridiqueUniteLegale());
		if(null !== $categorieJuridique)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_catjur_n3, $dateSince))
			{
				if(null === $legalUnit->insee_catjur_n3_id || (int) $legalUnit->insee_catjur_n3_id !== (int) $categorieJuridique)
				{
					$recordsToBeSaved[] = $this->handleHistoryInteger(InseeSireneLegalUnitHistoryCatjurN3::class, $legalUnit, (string) $legalUnit->date_since_catjur_n3, 'insee_catjur_n3_id');
					$legalUnit->insee_catjur_n3_id = (int) $categorieJuridique;
					$legalUnit->date_since_catjur_n3 = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$caractereEmployeur = $uniteLegale->hasCaractereEmployeurUniteLegale();
		if(null !== $caractereEmployeur)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_employer, $dateSince))
			{
				if(null === $legalUnit->is_employer || (int) $legalUnit->is_employer !== (int) $caractereEmployeur)
				{
					$recordsToBeSaved[] = $this->handleHistoryInteger(InseeSireneLegalUnitHistoryEmployer::class, $legalUnit, (string) $legalUnit->date_since_employer, 'is_employer');
					$legalUnit->is_employer = (int) $caractereEmployeur;
					$legalUnit->date_since_employer = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$denomination1 = $uniteLegale->getDenominationUsuelle1UniteLegale();
		$denomination2 = $uniteLegale->getDenominationUsuelle2UniteLegale();
		$denomination3 = $uniteLegale->getDenominationUsuelle3UniteLegale();
		if(null !== $denomination1 || null !== $denomination2 || null !== $denomination3)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_denomination, $dateSince))
			{
				$mustChangeDenom1 = null === $legalUnit->usage_denomination_1 || (string) $legalUnit->usage_denomination_1 !== (string) $denomination1;
				$mustChangeDenom2 = null === $legalUnit->usage_denomination_2 || (string) $legalUnit->usage_denomination_2 !== (string) $denomination2;
				$mustChangeDenom3 = null === $legalUnit->usage_denomination_3 || (string) $legalUnit->usage_denomination_3 !== (string) $denomination3;
				if($mustChangeDenom1 || $mustChangeDenom2 || $mustChangeDenom3)
				{
					$recordsToBeSaved[] = $this->handleHistoryStrings(InseeSireneLegalUnitHistoryDenomination::class, $legalUnit, (string) $legalUnit->date_since_denomination, [
						'usage_denomination_1', 'usage_denomination_2', 'usage_denomination_3',
					]);
					$mustBeSaved = false;
					if(null !== $denomination1)
					{
						$legalUnit->usage_denomination_1 = (string) $denomination1;
						$mustBeSaved = true;
					}
					if(null !== $denomination2)
					{
						$legalUnit->usage_denomination_2 = (string) $denomination2;
						$mustBeSaved = true;
					}
					if(null !== $denomination3)
					{
						$legalUnit->usage_denomination_3 = (string) $denomination3;
						$mustBeSaved = true;
					}
					if($mustBeSaved)
					{
						$legalUnit->date_since_denomination = $dateSince->format('Y-m-d');
					}
				}
			}
		}
		
		$etatAdministratif = $uniteLegale->getEtatAdministratifUniteLegale();
		if(null !== $etatAdministratif)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_etat_administratif, $dateSince))
			{
				if(null === $legalUnit->insee_sirene_etat_administratif_id || (int) $legalUnit->insee_sirene_etat_administratif_id !== (int) $etatAdministratif->getId())
				{
					$recordsToBeSaved[] = $this->handleHistoryInteger(InseeSireneLegalUnitHistoryEtatAdministratif::class, $legalUnit, (string) $legalUnit->date_since_etat_administratif, 'insee_sirene_etat_administratif_id');
					$legalUnit->insee_sirene_etat_administratif_id = (int) $etatAdministratif->getId();
					$legalUnit->date_since_etat_administratif = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$activitePrincipale = $uniteLegale->getActivitePrincipaleUniteLegale();
		$nomenclature = $uniteLegale->getNomenclatureActivitePrincipaleUniteLegale();
		if(null !== $activitePrincipale && '' !== $activitePrincipale)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_naf2008_lv5_subclass, $dateSince))
			{
				try
				{
					$activiteId = $this->_nafLoader->getNaf2008Lv5SubclassId($activitePrincipale, $nomenclature);
				}
				catch(InvalidArgumentException $exc)
				{
					$message = 'Failed to get naf2008 subclass with id {aid} and nomenclature {nom} : {exc}';
					$context = ['aid' => $activitePrincipale, 'nom' => $nomenclature, 'exc' => $exc->getMessage()];
					$this->_logger->warning($message, $context);
					$activiteId = null;
				}
				
				if(null !== $activiteId && (null === $legalUnit->insee_naf2008_lv5_subclass_id || (string) $legalUnit->insee_naf2008_lv5_subclass_id !== $activiteId))
				{
					$recordsToBeSaved[] = $this->handleHistoryString(InseeSireneLegalUnitHistoryNaf2008Lv5Subclass::class, $legalUnit, (string) $legalUnit->date_since_naf2008_lv5_subclass, 'insee_naf2008_lv5_subclass_id');
					$legalUnit->insee_naf2008_lv5_subclass_id = (string) $activiteId;
					$legalUnit->date_since_naf2008_lv5_subclass = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$economieSociale = $uniteLegale->hasEconomieSocialeSolidaireUniteLegale();
		if(null !== $economieSociale)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_social_economy, $dateSince))
			{
				if(null === $legalUnit->is_social_economy || (int) $legalUnit->is_social_economy !== (int) $economieSociale)
				{
					$recordsToBeSaved[] = $this->handleHistoryInteger(InseeSireneLegalUnitHistorySocialEconomy::class, $legalUnit, (string) $legalUnit->date_since_social_economy, 'is_social_economy');
					$legalUnit->is_social_economy = (int) $economieSociale;
					$legalUnit->date_since_social_economy = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$societeMission = $uniteLegale->hasSocieteMissionUniteLegale();
		if(null !== $societeMission)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_societe_mission, $dateSince))
			{
				if(null === $legalUnit->is_societe_mission || (int) $legalUnit->is_societe_mission !== (int) $societeMission)
				{
					$recordsToBeSaved[] = $this->handleHistoryInteger(InseeSireneLegalUnitHistorySocieteMission::class, $legalUnit, (string) $legalUnit->date_since_societe_mission, 'is_societe_mission');
					$legalUnit->is_societe_mission = (int) $societeMission;
					$legalUnit->date_since_societe_mission = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$trancheEffectif = $uniteLegale->getTrancheEffectifsUniteLegale();
		if(null !== $trancheEffectif)
		{
			if($this->isMoreRecentStr($legalUnit->date_since_tranche_effectif, $dateSince))
			{
				if(null === $legalUnit->insee_sirene_tranche_effectif_id || (int) $legalUnit->insee_sirene_tranche_effectif_id !== (int) $trancheEffectif->getId())
				{
					$recordsToBeSaved[] = $this->handleHistoryInteger(InseeSireneLegalUnitHistoryTrancheEffectif::class, $legalUnit, (string) $legalUnit->date_since_tranche_effectif, 'insee_sirene_tranche_effectif_id');
					$legalUnit->insee_sirene_tranche_effectif_id = (int) $trancheEffectif->getId();
					$legalUnit->date_since_tranche_effectif = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$count = (int) ($mustBeSaved && (bool) $this->saveRecord($legalUnit));
		
		foreach($recordsToBeSaved as $record)
		{
			if(null !== $record)
			{
				$count += $this->saveRecord($record);
			}
		}
		
		$count += $this->handleMoralPerson($legalUnit, $uniteLegale, $moralPerson, $dateSince);
		$count += $this->handlePhysicalPerson($legalUnit, $uniteLegale, $physicalPerson, $dateSince);
		
		return $count;
	}
	
	/**
	 * Handles the given history record with integer field values.
	 * 
	 * @template T of ActiveRecord
	 * @param class-string<T> $historyClassName
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param string $dateSince
	 * @param string $attributeName
	 * @return ?T
	 * @throws InvalidArgumentException
	 */
	public function handleHistoryInteger(string $historyClassName, InseeSireneLegalUnit $legalUnit, string $dateSince, string $attributeName) : ?ActiveRecord
	{
		$legalUnitValue = $legalUnit->getAttribute($attributeName);
		if(null === $legalUnitValue)
		{
			return null;
		}
		
		$history = $this->findOrCreate($historyClassName, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince,
		]);
		$historyValue = $history->getAttribute($attributeName);
		if(null === $historyValue || (int) $historyValue !== (int) $legalUnitValue)
		{
			try
			{
				$history->setAttribute($attributeName, $legalUnitValue);
			}
			catch(\yii\base\InvalidArgumentException $exc)
			{
				throw new InvalidArgumentException('Failed to set attribute '.$attributeName, -1, $exc);
			}
			
			return $history;
		}
		
		return null;
	}
	
	/**
	 * Handles the given history record with string field values.
	 *
	 * @template T of ActiveRecord
	 * @param class-string<T> $historyClassName
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param string $dateSince
	 * @param string $attributeName
	 * @return ?T
	 * @throws InvalidArgumentException
	 */
	public function handleHistoryString(string $historyClassName, InseeSireneLegalUnit $legalUnit, string $dateSince, string $attributeName) : ?ActiveRecord
	{
		$legalUnitValue = $legalUnit->getAttribute($attributeName);
		if(null === $legalUnitValue)
		{
			return null;
		}
		
		$history = $this->findOrCreate($historyClassName, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince,
		]);
		$historyValue = $history->getAttribute($attributeName);
		if(null === $historyValue || (string) $historyValue !== (string) $legalUnitValue)
		{
			try
			{
				$history->setAttribute($attributeName, $legalUnitValue);
			}
			catch(\yii\base\InvalidArgumentException $exc)
			{
				throw new InvalidArgumentException('Failed to set attribute '.$attributeName, -1, $exc);
			}
			
			return $history;
		}
		
		return null;
	}
	
	/**
	 * Handles the given history record with string field values for the given
	 * attributes.
	 *
	 * @template T of ActiveRecord
	 * @param class-string<T> $historyClassName
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param string $dateSince
	 * @param array<integer, string> $attributeNames
	 * @return ?T
	 * @throws InvalidArgumentException
	 */
	public function handleHistoryStrings(string $historyClassName, InseeSireneLegalUnit $legalUnit, string $dateSince, array $attributeNames) : ?ActiveRecord
	{
		$legalUnitValues = [];
		
		foreach($attributeNames as $attributeName)
		{
			$legalUnitValue = $legalUnit->getAttribute($attributeName);
			if(null !== $legalUnitValue)
			{
				$legalUnitValues[$attributeName] = $legalUnitValue;
			}
		}
		
		if(empty($legalUnitValues))
		{
			return null;
		}
		
		$history = $this->findOrCreate($historyClassName, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince,
		]);
		
		$hasChanged = false;
		
		foreach($attributeNames as $attributeName)
		{
			$legalUnitValue = $legalUnitValues[$attributeName] ?? null;
			$historyValue = $history->getAttribute($attributeName);
			if(null === $legalUnitValue || (string) $historyValue !== (string) $legalUnitValue)
			{
				try
				{
					$history->setAttribute($attributeName, $legalUnitValue);
				}
				catch(\yii\base\InvalidArgumentException $exc)
				{
					throw new InvalidArgumentException('Failed to set attribute '.((string) $attributeName), -1, $exc);
				}
				$hasChanged = true;
			}
		}
		
		return $hasChanged ? $history : null;
	}
	
	/**
	 * Handles the saving of moral person if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param ApiFrInseeSireneUniteLegaleInterface $uniteLegale
	 * @param ?InseeSireneMoralPerson $moralPerson
	 * @param DateTimeInterface $dateSince
	 * @return integer
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function handleMoralPerson(
		InseeSireneLegalUnit $legalUnit,
		ApiFrInseeSireneUniteLegaleInterface $uniteLegale,
		?InseeSireneMoralPerson $moralPerson,
		DateTimeInterface $dateSince
	) : int {
		
		$mustBeSaved = false;
		
		if(null === $moralPerson)
		{
			$moralPerson = new InseeSireneMoralPerson();
			$moralPerson->insee_sirene_legal_unit_id = $legalUnit->insee_sirene_legal_unit_id;
		}
		
		$denomination = $uniteLegale->getDenominationUniteLegale();
		if(null !== $denomination && '' !== $denomination)
		{
			if($this->isMoreRecentStr($moralPerson->date_since_raison_sociale, $dateSince))
			{
				if((string) $moralPerson->raison_sociale !== (string) $denomination)
				{
					$moralPerson->raison_sociale = (string) $denomination;
					$moralPerson->date_since_raison_sociale = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		$sigle = $uniteLegale->getSigleUniteLegale();
		if(null !== $sigle && '' !== $sigle)
		{
			if($this->isMoreRecentStr($moralPerson->date_since_sigle, $dateSince))
			{
				if((string) $moralPerson->sigle !== (string) $sigle)
				{
					$moralPerson->sigle = (string) $sigle;
					$moralPerson->date_since_sigle = $dateSince->format('Y-m-d');
					$mustBeSaved = true;
				}
			}
		}
		
		return (int) ($mustBeSaved && (bool) $this->saveRecord($legalUnit));
	}
	
	/**
	 * Handles the saving of physical person if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param ApiFrInseeSireneUniteLegaleInterface $uniteLegale
	 * @param ?InseeSirenePhysicalPerson $physicalPerson
	 * @param DateTimeInterface $dateSince
	 * @return integer
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function handlePhysicalPerson(
		InseeSireneLegalUnit $legalUnit,
		ApiFrInseeSireneUniteLegaleInterface $uniteLegale,
		?InseeSirenePhysicalPerson $physicalPerson,
		DateTimeInterface $dateSince
	) : int {
		
		$mustBeSaved = false;
		
		if(null === $physicalPerson)
		{
			$physicalPerson = new InseeSirenePhysicalPerson();
			$physicalPerson->insee_sirene_legal_unit_id = $legalUnit->insee_sirene_legal_unit_id;
		}
		
		$sexe = $uniteLegale->getSexeUniteLegale();
		if(!empty($sexe) && $sexe->getCode() !== $physicalPerson->gender)
		{
			$physicalPerson->gender = $sexe->getCode();
			$mustBeSaved = true;
		}
		
		$prenom1 = $uniteLegale->getPrenom1UniteLegale();
		if(null !== $prenom1 && '' !== $prenom1 && $prenom1 !== $physicalPerson->prenom_1)
		{
			$physicalPerson->prenom_1 = $uniteLegale->getPrenom1UniteLegale();
			$mustBeSaved = true;
		}
		
		$prenom2 = $uniteLegale->getPrenom2UniteLegale();
		if(null !== $prenom2 && '' !== $prenom2 && $prenom2 !== $physicalPerson->prenom_2)
		{
			$physicalPerson->prenom_2 = $uniteLegale->getPrenom2UniteLegale();
			$mustBeSaved = true;
		}
		
		$prenom3 = $uniteLegale->getPrenom3UniteLegale();
		if(null !== $prenom3 && '' !== $prenom3 && $prenom3 !== $physicalPerson->prenom_3)
		{
			$physicalPerson->prenom_3 = $uniteLegale->getPrenom3UniteLegale();
			$mustBeSaved = true;
		}
		
		$prenom4 = $uniteLegale->getPrenom4UniteLegale();
		if(null !== $prenom4 && '' !== $prenom4 && $prenom4 !== $physicalPerson->prenom_4)
		{
			$physicalPerson->prenom_4 = $uniteLegale->getPrenom4UniteLegale();
			$mustBeSaved = true;
		}
		
		$prenomUsuel = $uniteLegale->getPrenomUsuelUniteLegale();
		if(null !== $prenomUsuel && '' !== $prenomUsuel && $prenomUsuel !== $physicalPerson->usage_prenom)
		{
			$physicalPerson->usage_prenom = $uniteLegale->getPrenomUsuelUniteLegale();
			$mustBeSaved = true;
		}
		
		$pseudonyme = $uniteLegale->getPseudonymeUniteLegale();
		if(null !== $pseudonyme && '' !== $pseudonyme && $pseudonyme !== $physicalPerson->pseudonym)
		{
			$physicalPerson->pseudonym = $uniteLegale->getPseudonymeUniteLegale();
			$mustBeSaved = true;
		}
		
		$dateNameMustBeUpdated = false;
		$nom = $uniteLegale->getNomUniteLegale();
		if(null !== $nom && '' !== $nom)
		{
			if($this->isMoreRecentStr($physicalPerson->date_since_name, $dateSince))
			{
				if((string) $physicalPerson->name !== $nom)
				{
					$physicalPerson->name = $nom;
					$dateNameMustBeUpdated = true;
				}
			}
		}
		
		$nomUsage = $uniteLegale->getNomUsageUniteLegale();
		if(null !== $nomUsage && '' !== $nomUsage)
		{
			if($this->isMoreRecentStr($physicalPerson->date_since_name, $dateSince))
			{
				if((string) $physicalPerson->usage_name !== $nomUsage)
				{
					$physicalPerson->usage_name = $nomUsage;
					$dateNameMustBeUpdated = true;
				}
			}
		}
		
		if($dateNameMustBeUpdated)
		{
			$physicalPerson->date_since_name = $dateSince->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		return (int) ($mustBeSaved && (bool) $this->saveRecord($legalUnit));
	}
	
}
