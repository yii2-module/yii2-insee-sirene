<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneCategorieEntrepriseInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratifInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApeInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifsInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegaleHistorique;
use RuntimeException;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN3;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnit;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryCategorieEntreprise;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryCatjurN3;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryDenomination;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryEmployer;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistorySocialEconomy;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistorySocieteMission;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryTrancheEffectif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPerson;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPersonHistoryRaisonSociale;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPersonHistorySigle;
use Yii2Module\Yii2InseeSirene\Models\InseeSirenePhysicalPerson;
use Yii2Module\Yii2InseeSirene\Models\InseeSirenePhysicalPersonHistory;

/**
 * InseeSireneLegalUnitManager class file.
 * 
 * This class updates the legal units and their history and derivated records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class InseeSireneLegalUnitManager extends InseeSireneRecordManager
{
	
	/**
	 * Gets the juridic category id.
	 *
	 * @param ?string $catjur
	 * @return ?integer
	 */
	public function getCatjurId(?string $catjur) : ?int
	{
		if(null === $catjur || \trim($catjur) === '')
		{
			return null;
		}
		
		$catjur = (int) $catjur;
		
		$cjids = [
			2100 => 2110,
			2200 => 2210,
			2300 => 2310,
			3100 => 3110,
			3200 => 3205,
			3208 => 3210,
			3299 => 3290,
			4100 => 4110,
			5100 => 5191,
			5200 => 5202,
			5300 => 5306,
			5400 => 5410,
			5500 => 5505,
			5600 => 5605,
			5669 => 5660,
			6200 => 6210,
			6300 => 6316,
			6332 => 6317,
			6400 => 6411,
			6500 => 6511,
			7100 => 7111,
			7200 => 7210,
			7300 => 7312,
			8100 => 8110,
			8200 => 8210,
			8300 => 8310,
			8400 => 8410,
			9100 => 9110,
			9200 => 9210,
		];
		
		if(isset($cjids[$catjur]))
		{
			$catjur = $cjids[$catjur];
		}
		
		return $catjur;
	}
	
	/**
	 * Checks for all nomenclature activite ids.
	 *
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @throws RuntimeException
	 */
	public function checkNomenclatureActivite(ApiFrInseeSireneEndpointInterface $endpoint) : void
	{
		$this->_logger->info('Checking Sirene Legal Unit Histories for nomenclature activites ids');
		
		/** @var ApiFrInseeSireneUniteLegaleHistorique $legalUnitHistory */
		foreach($endpoint->getLatestStockUniteLegaleHistoricIterator() as $k => $legalUnitHistory)
		{
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Legal Unit Lines...', ['k' => (int) $k + 1]);
			}
			
			try
			{
				$this->_nafLoader->getNaf2008Lv5SubclassId(
					$legalUnitHistory->getActivitePrincipaleUniteLegale(),
					$legalUnitHistory->getNomenclatureActivitePrincipaleUniteLegale(),
				);
			}
			catch(InvalidArgumentException $exc)
			{
				$this->_logger->warning('Failed to find activite with id {id} in {nomenclature}', [
					'id' => $legalUnitHistory->getActivitePrincipaleUniteLegale(),
					'nomenclature' => $legalUnitHistory->getNomenclatureActivitePrincipaleUniteLegale(),
				]);
			}
		}
	}
	
	/**
	 * Checks for all juridic category ids.
	 *
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @throws RuntimeException
	 */
	public function checkCatjurId(ApiFrInseeSireneEndpointInterface $endpoint) : void
	{
		$this->_logger->info('Checking Sirene Legal Units for catjur ids');
		
		/** @var ApiFrInseeSireneUniteLegaleHistorique $legalUnitHist */
		foreach($endpoint->getLatestStockUniteLegaleHistoricIterator() as $k => $legalUnitHist)
		{
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Legal Unit History Lines...', ['{k}' => (int) $k + 1]);
			}
			
			$catjurid = $this->getCatjurId($legalUnitHist->getCategorieJuridiqueUniteLegale());
			if(null === $catjurid)
			{
				continue;
			}
			
			$catjur = InseeCatjurN3::findOne($catjurid);
			if(null === $catjur)
			{
				$this->_logger->info('Failed to find juridic category with id {id}', ['id' => $catjurid]);
			}
		}
	}
	
	/**
	 * Handles the saving of the categorie entreprise if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?ApiFrInseeSireneCategorieEntrepriseInterface $categorieEntreprise
	 * @return ?InseeSireneLegalUnitHistoryCategorieEntreprise
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistoryCategoryEntreprise(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?ApiFrInseeSireneCategorieEntrepriseInterface $categorieEntreprise
	) : ?InseeSireneLegalUnitHistoryCategorieEntreprise {
		
		if(null === $categorieEntreprise)
		{
			return null;
		}
		
		if($categorieEntreprise->getId() === $legalUnit->insee_sirene_category_entreprise_id)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_category_entreprise)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_category_entreprise || null === $legalUnit->insee_sirene_category_entreprise_id)
		{
			$legalUnit->insee_sirene_category_entreprise_id = $categorieEntreprise->getId();
			$legalUnit->date_since_category_entreprise = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_category_entreprise, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistoryCategorieEntreprise::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_category_entreprise,
			]);
			$record->insee_sirene_categorie_entreprise_id = $legalUnit->insee_sirene_category_entreprise_id;
			$legalUnit->insee_sirene_category_entreprise_id = $categorieEntreprise->getId();
			$legalUnit->date_since_category_entreprise = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistoryCategorieEntreprise::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_sirene_categorie_entreprise_id = (int) $categorieEntreprise->getId();
		
		return $record;
	}
	
	/**
	 * Handles the saving of the categorie juridique if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?string $categorieJuridique
	 * @return ?InseeSireneLegalUnitHistoryCatjurN3
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistoryCategorieJuridique(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?string $categorieJuridique
	) : ?InseeSireneLegalUnitHistoryCatjurN3 {
		
		$categorieJuridique = $this->getCatjurId($categorieJuridique);
		if(null === $categorieJuridique)
		{
			return null;
		}
		
		if($legalUnit->insee_catjur_n3_id === $categorieJuridique)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_catjur_n3)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_catjur_n3 || null === $legalUnit->insee_catjur_n3_id)
		{
			$legalUnit->insee_catjur_n3_id = $categorieJuridique;
			$legalUnit->date_since_catjur_n3 = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_catjur_n3, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistoryCatjurN3::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_catjur_n3,
			]);
			$record->insee_catjur_n3_id = $legalUnit->insee_catjur_n3_id;
			$legalUnit->insee_catjur_n3_id = $categorieJuridique;
			$legalUnit->date_since_catjur_n3 = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistoryCatjurN3::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_catjur_n3_id = $categorieJuridique;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the caractere employer if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?boolean $caractereEmployeur
	 * @return ?InseeSireneLegalUnitHistoryEmployer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistoryCaractereEmployeur(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?bool $caractereEmployeur
	) : ?InseeSireneLegalUnitHistoryEmployer {
		
		if(null === $caractereEmployeur)
		{
			return null;
		}
		
		if((int) $caractereEmployeur === $legalUnit->is_employer)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_employer)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_employer || null === $legalUnit->is_employer)
		{
			$legalUnit->is_employer = (int) $caractereEmployeur;
			$legalUnit->date_since_employer = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_employer, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistoryEmployer::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_employer,
			]);
			$record->is_employer = (int) $legalUnit->is_employer;
			$legalUnit->is_employer = (int) $caractereEmployeur;
			$legalUnit->date_since_employer = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistoryEmployer::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->is_employer = (int) $caractereEmployeur;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the denomination if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?string $denomination1
	 * @param ?string $denomination2
	 * @param ?string $denomination3
	 * @return ?InseeSireneLegalUnitHistoryDenomination
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistoryDenomination(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?string $denomination1,
		?string $denomination2,
		?string $denomination3
	) : ?InseeSireneLegalUnitHistoryDenomination {
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_denomination)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_denomination
			|| (
				null === $legalUnit->usage_denomination_1
				&& null === $legalUnit->usage_denomination_2
				&& null === $legalUnit->usage_denomination_3
			)
		) {
			if(null !== $denomination1 && '' !== $denomination1)
			{
				$legalUnit->usage_denomination_1 = $denomination1;
			}
			if(null !== $denomination2 && '' !== $denomination2)
			{
				$legalUnit->usage_denomination_2 = $denomination2;
			}
			if(null !== $denomination3 && '' !== $denomination3)
			{
				$legalUnit->usage_denomination_3 = $denomination3;
			}
			$legalUnit->date_since_denomination = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_denomination, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistoryDenomination::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_denomination,
			]);
			$record->usage_denomination_1 = (string) $legalUnit->usage_denomination_1;
			$record->usage_denomination_2 = $legalUnit->usage_denomination_2;
			$record->usage_denomination_3 = $legalUnit->usage_denomination_3;
			$legalUnit->usage_denomination_1 = null === $denomination1 || '' === $denomination1 ? null : $denomination1;
			$legalUnit->usage_denomination_2 = null === $denomination2 || '' === $denomination2 ? null : $denomination2;
			$legalUnit->usage_denomination_3 = null === $denomination3 || '' === $denomination3 ? null : $denomination3;
			$legalUnit->date_since_denomination = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistoryDenomination::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		if(null !== $denomination1 && '' !== $denomination1)
		{
			$record->usage_denomination_1 = $denomination1;
		}
		if(null !== $denomination2 && '' !== $denomination2)
		{
			$record->usage_denomination_2 = $denomination2;
		}
		if(null !== $denomination3 && '' !== $denomination3)
		{
			$record->usage_denomination_3 = $denomination3;
		}
		
		return $record;
	}
	
	/**
	 * Handles the saving of the etat administratif if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratif
	 * @return ?InseeSireneLegalUnitHistoryEtatAdministratif
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistoryEtatAdministratif(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratif
	) : ?InseeSireneLegalUnitHistoryEtatAdministratif {
		
		if(null === $etatAdministratif)
		{
			return null;
		}
		
		if($etatAdministratif->getId() === $legalUnit->insee_sirene_etat_administratif_id)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_etat_administratif)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_etat_administratif || null === $legalUnit->insee_sirene_etat_administratif_id)
		{
			$legalUnit->insee_sirene_etat_administratif_id = $etatAdministratif->getId();
			$legalUnit->date_since_etat_administratif = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_etat_administratif, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistoryEtatAdministratif::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_etat_administratif,
			]);
			$record->insee_sirene_etat_administratif_id = $legalUnit->insee_sirene_etat_administratif_id;
			$legalUnit->insee_sirene_etat_administratif_id = $etatAdministratif->getId();
			$legalUnit->date_since_etat_administratif = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistoryEtatAdministratif::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_sirene_etat_administratif_id = (int) $etatAdministratif->getId();
		
		return $record;
	}
	
	/**
	 * Handles the saving of the naf 2008 lv5 (activite) if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?string $activite
	 * @param ?ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	 * @return ?InseeSireneLegalUnitHistoryNaf2008Lv5Subclass
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistoryNaf2008Lv5Subclass(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?string $activite,
		?ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	) : ?InseeSireneLegalUnitHistoryNaf2008Lv5Subclass {
		
		if(null === $activite || '' === $activite)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_naf2008_lv5_subclass)
		{
			return null;
		}
		
		try
		{
			$activiteId = $this->_nafLoader->getNaf2008Lv5SubclassId($activite, $nomenclature);
			if(null === $activiteId)
			{
				return null;
			}
		}
		catch(InvalidArgumentException $e)
		{
			$message = 'Failed to transform activite "{act}" in nomenclature "{nom}" into a suitable value';
			$context = ['{act}' => $activite, '{nom}' => $nomenclature];
			
			throw new RuntimeException(\strtr($message, $context), -1, $e);
		}
		
		if($legalUnit->insee_naf2008_lv5_subclass_id === $activiteId)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_naf2008_lv5_subclass || null === $legalUnit->insee_naf2008_lv5_subclass_id)
		{
			$legalUnit->insee_naf2008_lv5_subclass_id = $activiteId;
			$legalUnit->date_since_naf2008_lv5_subclass = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_naf2008_lv5_subclass, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistoryNaf2008Lv5Subclass::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_naf2008_lv5_subclass,
			]);
			$record->insee_naf2008_lv5_subclass_id = $legalUnit->insee_naf2008_lv5_subclass_id;
			$legalUnit->insee_naf2008_lv5_subclass_id = $activiteId;
			$legalUnit->date_since_naf2008_lv5_subclass = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistoryNaf2008Lv5Subclass::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_naf2008_lv5_subclass_id = $activiteId;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the social economy if needed.
	 *
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?bool $isSocialEconomy
	 * @return ?InseeSireneLegalUnitHistorySocialEconomy
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistorySocialEconomy(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?bool $isSocialEconomy
	) : ?InseeSireneLegalUnitHistorySocialEconomy {
		
		if(null === $isSocialEconomy)
		{
			return null;
		}
		
		if((int) $isSocialEconomy === $legalUnit->is_social_economy)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_social_economy)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_social_economy || null === $legalUnit->is_social_economy)
		{
			$legalUnit->is_social_economy = (int) $isSocialEconomy;
			$legalUnit->date_since_social_economy = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_social_economy, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistorySocialEconomy::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_social_economy,
			]);
			$record->is_social_economy = $legalUnit->is_social_economy;
			$legalUnit->is_social_economy = (int) $isSocialEconomy;
			$legalUnit->date_since_social_economy = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistorySocialEconomy::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->is_social_economy = (int) $isSocialEconomy;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the societe mission if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?boolean $isSocieteMission
	 * @return ?InseeSireneLegalUnitHistorySocieteMission
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistorySocieteMission(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?bool $isSocieteMission
	) : ?InseeSireneLegalUnitHistorySocieteMission {
		
		if(null === $isSocieteMission)
		{
			return null;
		}
		
		if((int) $isSocieteMission === $legalUnit->is_societe_mission)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_societe_mission)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_societe_mission || null === $legalUnit->is_societe_mission)
		{
			$legalUnit->is_societe_mission = (int) $isSocieteMission;
			$legalUnit->date_since_societe_mission = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_societe_mission, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistorySocieteMission::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_societe_mission,
			]);
			$record->is_societe_mission = $legalUnit->is_societe_mission;
			$legalUnit->is_societe_mission = (int) $isSocieteMission;
			$legalUnit->date_since_societe_mission = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistorySocieteMission::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->is_societe_mission = (int) $isSocieteMission;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the tranche effectif if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param DateTimeInterface $dateSince
	 * @param ?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectif
	 * @return ?InseeSireneLegalUnitHistoryTrancheEffectif
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleLegalUnitHistoryTrancheEffectif(
		InseeSireneLegalUnit $legalUnit,
		DateTimeInterface $dateSince,
		?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectif
	) : ?InseeSireneLegalUnitHistoryTrancheEffectif {
		
		if(null === $trancheEffectif)
		{
			return null;
		}
		
		if($trancheEffectif->getId() === $legalUnit->insee_sirene_tranche_effectif_id)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $legalUnit->date_since_tranche_effectif)
		{
			return null;
		}
		
		if(null === $legalUnit->date_since_tranche_effectif || null === $legalUnit->insee_sirene_tranche_effectif_id)
		{
			$legalUnit->insee_sirene_tranche_effectif_id = $trancheEffectif->getId();
			$legalUnit->date_since_tranche_effectif = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($legalUnit->date_since_tranche_effectif, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneLegalUnitHistoryTrancheEffectif::class, [
				'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
				'date_since' => $legalUnit->date_since_tranche_effectif,
			]);
			$record->insee_sirene_tranche_effectif_id = $legalUnit->insee_sirene_tranche_effectif_id;
			$legalUnit->insee_sirene_tranche_effectif_id = $trancheEffectif->getId();
			$legalUnit->date_since_tranche_effectif = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneLegalUnitHistoryTrancheEffectif::class, [
			'insee_sirene_legal_unit_id' => $legalUnit->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_sirene_tranche_effectif_id = (int) $trancheEffectif->getId();
		
		return $record;
	}
	
	/**
	 * Handles the saving of moral person history raison sociale if needed.
	 * 
	 * @param InseeSireneMoralPerson $moralPerson
	 * @param DateTimeInterface $dateSince
	 * @param ?string $denomination
	 * @return ?InseeSireneMoralPersonHistoryRaisonSociale
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleMoralPersonHistoryRaisonSociale(
		InseeSireneMoralPerson $moralPerson,
		DateTimeInterface $dateSince,
		?string $denomination
	) : ?InseeSireneMoralPersonHistoryRaisonSociale {
		
		if(null === $denomination || '' === $denomination)
		{
			return null;
		}
		
		if($moralPerson->raison_sociale === $denomination)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $moralPerson->date_since_raison_sociale)
		{
			return null;
		}
		
		if(null === $moralPerson->date_since_raison_sociale || null === $moralPerson->raison_sociale)
		{
			$moralPerson->raison_sociale = $denomination;
			$moralPerson->date_since_raison_sociale = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($moralPerson->date_since_raison_sociale, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneMoralPersonHistoryRaisonSociale::class, [
				'insee_sirene_legal_unit_id' => $moralPerson->insee_sirene_legal_unit_id,
				'date_since' => $moralPerson->date_since_raison_sociale,
			]);
			$record->raison_sociale = $moralPerson->raison_sociale;
			$moralPerson->raison_sociale = $denomination;
			$moralPerson->date_since_raison_sociale = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneMoralPersonHistoryRaisonSociale::class, [
			'insee_sirene_legal_unit_id' => $moralPerson->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->raison_sociale = $denomination;
		
		return $record;
	}
	
	/**
	 * Handles the saving of physical person if needed.
	 * 
	 * @param InseeSireneMoralPerson $moralPerson
	 * @param DateTimeInterface $dateSince
	 * @param ?string $sigle
	 * @return ?InseeSireneMoralPersonHistorySigle
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleMoralPersonHistorySigle(
		InseeSireneMoralPerson $moralPerson,
		DateTimeInterface $dateSince,
		?string $sigle
	) : ?InseeSireneMoralPersonHistorySigle {
		
		if(null === $sigle || '' === $sigle)
		{
			return null;
		}
		
		if($moralPerson->sigle === $sigle)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $moralPerson->date_since_sigle)
		{
			return null;
		}
		
		if(null === $moralPerson->date_since_sigle || null === $moralPerson->sigle)
		{
			$moralPerson->sigle = $sigle;
			$moralPerson->date_since_sigle = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($moralPerson->date_since_sigle, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneMoralPersonHistorySigle::class, [
				'insee_sirene_legal_unit_id' => $moralPerson->insee_sirene_legal_unit_id,
				'date_since' => $moralPerson->date_since_sigle,
			]);
			$record->sigle = $moralPerson->sigle;
			$moralPerson->sigle = $sigle;
			$moralPerson->date_since_sigle = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneMoralPersonHistorySigle::class, [
			'insee_sirene_legal_unit_id' => $moralPerson->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->sigle = $sigle;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the physical person history if needed.
	 * 
	 * @param InseeSirenePhysicalPerson $physicalPerson
	 * @param DateTimeInterface $dateSince
	 * @param ?string $nom
	 * @param ?string $nomUsage
	 * @return ?InseeSirenePhysicalPersonHistory
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handlePhysicalPersonHistory(
		InseeSirenePhysicalPerson $physicalPerson,
		DateTimeInterface $dateSince,
		?string $nom,
		?string $nomUsage
	) : ?InseeSirenePhysicalPersonHistory {
		
		if($dateSince->format('Y-m-d') === $physicalPerson->date_since_name)
		{
			return null;
		}
		
		if(null === $physicalPerson->date_since_name
			|| (null === $physicalPerson->name && null === $physicalPerson->usage_name)
		) {
			if(null !== $nom && '' !== $nom)
			{
				$physicalPerson->name = $nom;
			}
			if(null !== $nomUsage && '' !== $nomUsage)
			{
				$physicalPerson->usage_name = $nomUsage;
			}
			
			if(null === $physicalPerson->usage_name || '' === $physicalPerson->usage_name)
			{
				$physicalPerson->usage_name = (string) $physicalPerson->name;
			}
			
			$physicalPerson->date_since_name = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($physicalPerson->date_since_name, $dateSince))
		{
			$record = $this->findOrCreate(InseeSirenePhysicalPersonHistory::class, [
				'insee_sirene_legal_unit_id' => $physicalPerson->insee_sirene_legal_unit_id,
				'date_since' => $physicalPerson->date_since_name,
			]);
			$record->name = $physicalPerson->name;
			$record->usage_name = (string) $physicalPerson->usage_name;
			if(empty($record->usage_name))
			{
				$record->usage_name = (string) $record->name;
			}
			if(null !== $nom && '' !== $nom)
			{
				$physicalPerson->name = $nom;
			}
			if(null !== $nomUsage && '' !== $nomUsage)
			{
				$physicalPerson->usage_name = $nomUsage;
			}
			
			if(null === $physicalPerson->usage_name || '' === $physicalPerson->usage_name)
			{
				$physicalPerson->usage_name = (string) $physicalPerson->name;
			}
			
			$physicalPerson->date_since_name = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSirenePhysicalPersonHistory::class, [
			'insee_sirene_legal_unit_id' => $physicalPerson->insee_sirene_legal_unit_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		
		if(null !== $nom && '' !== $nom)
		{
			$record->name = $nom;
		}
		if(null !== $nomUsage && '' !== $nomUsage)
		{
			$record->usage_name = (string) $nomUsage;
		}
		if(empty($record->usage_name))
		{
			$record->usage_name = (string) $record->name;
		}
		
		return $record;
	}
	
}
