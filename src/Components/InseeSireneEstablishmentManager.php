<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratifInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApeInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifsInterface;
use RuntimeException;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishment;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryDenomination;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEmployer;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEnseigne;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryNaf2008Lv6Artisanat;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistorySiege;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryTrancheEffectif;

/**
 * InseeSireneEstablishmentManager class file.
 * 
 * This class updates the establishments and their history and derivated records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeSireneEstablishmentManager extends InseeSireneRecordManager
{
	
	/**
	 * Handles the saving of the denomination if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?string $denomination
	 * @return ?InseeSireneEstablishmentHistoryDenomination
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentHistoryDenomination(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?string $denomination
	) : ?InseeSireneEstablishmentHistoryDenomination {
		
		if(null === $denomination || '' === $denomination)
		{
			return null;
		}
		
		if($denomination === $establishment->usage_denomination)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $establishment->date_since_usage_denomination)
		{
			return null;
		}
		
		if(null === $establishment->date_since_usage_denomination || null === $establishment->usage_denomination)
		{
			$establishment->usage_denomination = $denomination;
			$establishment->date_since_usage_denomination = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_usage_denomination, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistoryDenomination::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_usage_denomination,
			]);
			$record->usage_denomination = $establishment->usage_denomination;
			$establishment->usage_denomination = $denomination;
			$establishment->date_since_usage_denomination = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistoryDenomination::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->usage_denomination = $denomination;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the caractere employer if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?bool $caractereEmployeur
	 * @return ?InseeSireneEstablishmentHistoryEmployer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentHistoryCaractereEmployeur(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?bool $caractereEmployeur
	) : ?InseeSireneEstablishmentHistoryEmployer {
		
		if(null === $caractereEmployeur)
		{
			return null;
		}
		
		if((int) $caractereEmployeur === $establishment->is_employer)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $establishment->date_since_is_employer)
		{
			return null;
		}
		
		if(null === $establishment->date_since_is_employer || null === $establishment->is_employer)
		{
			$establishment->is_employer = (int) $caractereEmployeur;
			$establishment->date_since_is_employer = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_is_employer, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistoryEmployer::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_is_employer,
			]);
			$record->is_employer = (int) $establishment->is_employer;
			$establishment->is_employer = (int) $caractereEmployeur;
			$establishment->date_since_is_employer = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistoryEmployer::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->is_employer = (int) $caractereEmployeur;
		
		return $record;
	}
	
	/**
	 * Handles the saving of enseignes if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?string $enseigne1
	 * @param ?string $enseigne2
	 * @param ?string $enseigne3
	 * @return ?InseeSireneEstablishmentHistoryEnseigne
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentEnseigne(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?string $enseigne1,
		?string $enseigne2,
		?string $enseigne3
	) : ?InseeSireneEstablishmentHistoryEnseigne {
		if($dateSince->format('Y-m-d') === $establishment->date_since_enseigne)
		{
			return null;
		}
		
		if(null === $establishment->date_since_enseigne
			|| (
				null === $establishment->enseigne_1
				&& null === $establishment->enseigne_2
				&& null === $establishment->enseigne_3
			)
		) {
			if(null !== $enseigne1 && '' !== $enseigne1)
			{
				$establishment->enseigne_1 = $enseigne1;
			}
			if(null !== $enseigne2 && '' !== $enseigne2)
			{
				$establishment->enseigne_2 = $enseigne2;
			}
			if(null !== $enseigne3 && '' !== $enseigne3)
			{
				$establishment->enseigne_3 = $enseigne3;
			}
			$establishment->date_since_enseigne = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_enseigne, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistoryEnseigne::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_enseigne,
			]);
			$record->enseigne_1 = (string) $establishment->enseigne_1;
			$record->enseigne_2 = $establishment->enseigne_2;
			$record->enseigne_3 = $establishment->enseigne_3;
			$establishment->enseigne_1 = null === $enseigne1 || '' === $enseigne1 ? null : $enseigne1;
			$establishment->enseigne_2 = null === $enseigne2 || '' === $enseigne2 ? null : $enseigne2;
			$establishment->enseigne_3 = null === $enseigne3 || '' === $enseigne3 ? null : $enseigne3;
			$establishment->date_since_enseigne = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistoryEnseigne::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		if(null !== $enseigne1 && '' !== $enseigne1)
		{
			$record->enseigne_1 = $enseigne1;
		}
		if(null !== $enseigne2 && '' !== $enseigne2)
		{
			$record->enseigne_2 = $enseigne2;
		}
		if(null !== $enseigne3 && '' !== $enseigne3)
		{
			$record->enseigne_3 = $enseigne3;
		}
		
		return $record;
	}
	
	/**
	 * Handles the saving of etat administratif if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratif
	 * @return ?InseeSireneEstablishmentHistoryEtatAdministratif
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentEtatAdministratif(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratif
	) : ?InseeSireneEstablishmentHistoryEtatAdministratif {
		
		if(null === $etatAdministratif)
		{
			return null;
		}
		
		if($etatAdministratif->getId() === $establishment->insee_sirene_etat_administratif_id)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $establishment->date_since_etat_administratif)
		{
			return null;
		}
		
		if(null === $establishment->date_since_etat_administratif || null === $establishment->insee_sirene_etat_administratif_id)
		{
			$establishment->insee_sirene_etat_administratif_id = $etatAdministratif->getId();
			$establishment->date_since_etat_administratif = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_etat_administratif, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistoryEtatAdministratif::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_etat_administratif,
			]);
			$record->insee_sirene_etat_administratif_id = $establishment->insee_sirene_etat_administratif_id;
			$establishment->insee_sirene_etat_administratif_id = $etatAdministratif->getId();
			$establishment->date_since_etat_administratif = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistoryEtatAdministratif::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_sirene_etat_administratif_id = (int) $etatAdministratif->getId();
		
		return $record;
	}
	
	/**
	 * Handles the saving of activites if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?string $activite
	 * @param ?ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	 * @return ?InseeSireneEstablishmentHistoryNaf2008Lv5Subclass
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentNaf2008Lv5Subclass(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?string $activite,
		?ApiFrInseeSireneNomenclatureApeInterface $nomenclature
	) : ?InseeSireneEstablishmentHistoryNaf2008Lv5Subclass {
		
		if(null === $activite || '' === $activite)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $establishment->date_since_naf2008_lv5_subclass)
		{
			return null;
		}
		
		try
		{
			$activiteId = $this->_nafLoader->getNaf2008Lv5SubclassId($activite, $nomenclature);
			if(null === $activiteId)
			{
				return null;
			}
		}
		catch(InvalidArgumentException $e)
		{
			$message = 'Failed to transform activite "{act}" in nomenclature "{nom}" into a suitable value';
			$context = ['{act}' => $activite, '{nom}' => $nomenclature];
			
			throw new RuntimeException(\strtr($message, $context), -1, $e);
		}
		
		if($establishment->insee_naf2008_lv5_subclass_id === $activiteId)
		{
			return null;
		}
		
		if(null === $establishment->date_since_naf2008_lv5_subclass || null === $establishment->insee_naf2008_lv5_subclass_id)
		{
			$establishment->insee_naf2008_lv5_subclass_id = $activiteId;
			$establishment->date_since_naf2008_lv5_subclass = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_naf2008_lv5_subclass, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistoryNaf2008Lv5Subclass::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_naf2008_lv5_subclass,
			]);
			$record->insee_naf2008_lv5_subclass_id = $establishment->insee_naf2008_lv5_subclass_id;
			$establishment->insee_naf2008_lv5_subclass_id = $activiteId;
			$establishment->date_since_naf2008_lv5_subclass = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistoryNaf2008Lv5Subclass::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_naf2008_lv5_subclass_id = $activiteId;
		
		return $record;
	}
	
	/**
	 * Handles the saving of artisanats if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?string $artisanat
	 * @return ?InseeSireneEstablishmentHistoryNaf2008Lv6Artisanat
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentNaf2008Lv6Artisanat(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?string $artisanat
	) : ?InseeSireneEstablishmentHistoryNaf2008Lv6Artisanat {
		
		if(null === $artisanat || '' === $artisanat)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $establishment->date_since_naf2008_lv6_artisanat)
		{
			return null;
		}
		
		$artisanatId = $this->_nafLoader->getNaf2008Lv6ArtisanatId($artisanat);
		if(null === $artisanatId)
		{
			return null;
		}
		
		if($establishment->insee_naf2008_lv6_artisanat_id === $artisanatId)
		{
			return null;
		}
		
		if(null === $establishment->date_since_naf2008_lv6_artisanat || null === $establishment->insee_naf2008_lv6_artisanat_id)
		{
			$establishment->insee_naf2008_lv6_artisanat_id = $artisanatId;
			$establishment->date_since_naf2008_lv6_artisanat = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_naf2008_lv6_artisanat, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistoryNaf2008Lv6Artisanat::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_naf2008_lv6_artisanat,
			]);
			$record->insee_naf2008_lv6_artisanat_id = $establishment->insee_naf2008_lv6_artisanat_id;
			$establishment->insee_naf2008_lv6_artisanat_id = $artisanatId;
			$establishment->date_since_naf2008_lv6_artisanat = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistoryNaf2008Lv6Artisanat::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_naf2008_lv6_artisanat_id = $artisanatId;
		
		return $record;
	}
	
	/**
	 * Handles the saving of siege status if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?bool $isSiege
	 * @return ?InseeSireneEstablishmentHistorySiege
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentSiege(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?bool $isSiege
	) : ?InseeSireneEstablishmentHistorySiege {
		if(null === $isSiege)
		{
			return null;
		}
		
		if((int) $isSiege === $establishment->is_siege)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $establishment->date_since_is_siege)
		{
			return null;
		}
		
		if(null === $establishment->date_since_is_siege || null === $establishment->is_siege)
		{
			$establishment->is_siege = (int) $isSiege;
			$establishment->date_since_is_siege = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_is_siege, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistorySiege::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_is_siege,
			]);
			$record->is_siege = (int) $establishment->is_siege;
			$establishment->is_siege = (int) $isSiege;
			$establishment->date_since_is_siege = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistorySiege::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $establishment->date_since_is_siege,
		]);
		$record->is_siege = (int) $isSiege;
		
		return $record;
	}
	
	/**
	 * Handles the saving of the tranche effectif if needed.
	 * 
	 * @param InseeSireneEstablishment $establishment
	 * @param DateTimeInterface $dateSince
	 * @param ?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectif
	 * @return ?InseeSireneEstablishmentHistoryTrancheEffectif
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleEstablishmentTrancheEffectif(
		InseeSireneEstablishment $establishment,
		DateTimeInterface $dateSince,
		?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectif
	) : ?InseeSireneEstablishmentHistoryTrancheEffectif {
		if(null === $trancheEffectif)
		{
			return null;
		}
		
		if($trancheEffectif->getId() === $establishment->insee_sirene_tranche_effectif_id)
		{
			return null;
		}
		
		if($dateSince->format('Y-m-d') === $establishment->date_since_tranche_effectif)
		{
			return null;
		}
		
		if(null === $establishment->date_since_tranche_effectif || null === $establishment->insee_sirene_tranche_effectif_id)
		{
			$establishment->insee_sirene_tranche_effectif_id = $trancheEffectif->getId();
			$establishment->date_since_tranche_effectif = $dateSince->format('Y-m-d');
			
			return null;
		}
		
		if($this->isMoreRecentStr($establishment->date_since_tranche_effectif, $dateSince))
		{
			$record = $this->findOrCreate(InseeSireneEstablishmentHistoryTrancheEffectif::class, [
				'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
				'date_since' => $establishment->date_since_tranche_effectif,
			]);
			$record->insee_sirene_tranche_effectif_id = (int) $establishment->insee_sirene_tranche_effectif_id;
			$establishment->insee_sirene_tranche_effectif_id = $trancheEffectif->getId();
			$establishment->date_since_tranche_effectif = $dateSince->format('Y-m-d');
			
			return $record;
		}
		
		$record = $this->findOrCreate(InseeSireneEstablishmentHistoryTrancheEffectif::class, [
			'insee_sirene_establishment_id' => $establishment->insee_sirene_establishment_id,
			'date_since' => $dateSince->format('Y-m-d'),
		]);
		$record->insee_sirene_tranche_effectif_id = (int) $trancheEffectif->getId();
		
		return $record;
	}
	
}
