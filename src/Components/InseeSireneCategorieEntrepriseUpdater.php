<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneCategorieEntreprise;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use RuntimeException;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneCategorieEntreprise;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;

/**
 * InseeSireneCategorieEntrepriseUpdater class file.
 * 
 * This class updates the InseeSireneCategorieEntreprise records.
 * 
 * @author Anastaszor
 */
class InseeSireneCategorieEntrepriseUpdater extends InseeSireneRecordManager
{
	
	/**
	 * Updates all the sirene categorie entreprises.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force
	 * @return integer the number of records saved
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Categories Entreprises');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_categorie_entreprise');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$count = 0;
		
		/** @var ApiFrInseeSireneCategorieEntreprise $categorieEntreprise */
		foreach($endpoint->getCategorieEntrepriseIterator() as $k => $categorieEntreprise)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} Categories Entreprises', ['k' => (int) $k + 1]);
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			$count += (int) $this->saveObjectClass(InseeSireneCategorieEntreprise::class, [
				'insee_sirene_categorie_entreprise_id' => (int) $categorieEntreprise->getId(),
			], [
				'code' => $categorieEntreprise->getCode(),
				'description' => $categorieEntreprise->getName(),
			])->isNewRecord;
		}
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_categorie_entreprise';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$mdr->save();
		}
		
		return $count;
	}
	
}
