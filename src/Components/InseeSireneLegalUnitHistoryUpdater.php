<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegaleHistoriqueInterface;
use RuntimeException;
use yii\db\ActiveRecord;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnit;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPerson;
use Yii2Module\Yii2InseeSirene\Models\InseeSirenePhysicalPerson;

/**
 * InseeSireneLegalUnitHistoryUpdater class file.
 * 
 * This class updates the InseeSireneLegalUnitHistory and derivated records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class InseeSireneLegalUnitHistoryUpdater extends InseeSireneLegalUnitManager
{
	
	public const BATCH_SIZE = 500;
	
	/**
	 * Gets the most recent datetime from unite legale, or a default one if none.
	 *
	 * @param ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale
	 * @return DateTimeInterface
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getDateSince(ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale) : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return $uniteLegale->getDateDebut() ?? DateTimeImmutable::createFromFormat('Y-m-d', '1900-01-01');
	}
	
	/**
	 * Updates all the sirene legal unit histories.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Legal Unit History');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_legal_unit_history');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_sirene_legal_unit_history.'.$dbdate->format('Y-m');
		$mdc = InseeSireneMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeSireneMetadata();
			$mdc->insee_sirene_metadata_id = $mdcid;
			$mdc->contents = '0';
			$this->saveRecord($mdc);
		}
		
		$count = 0;
		
		foreach($endpoint->getLatestStockUniteLegaleHistoricIterator() as $k => $legalUnitHist)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Legal Unit History Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Legal Unit History Lines...', ['{k}' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$this->saveRecord($mdc);
				$this->keepalive();
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			try
			{
				$count += $this->updateLegalUnit($legalUnitHist);
			}
			catch(RuntimeException $exc)
			{
				$message = $exc->getMessage()."\n\n".$exc->getTraceAsString();
				$prev = $exc->getPrevious();
				
				while(null !== $prev)
				{
					$message .= "\n\n".$prev->getMessage()."\n\n".$prev->getTraceAsString();
					$prev = $prev->getPrevious();
				}
				$this->_logger->error($message);
			}
		}
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_legal_unit_history';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$this->saveRecord($mdr);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene legal unit history.
	 * 
	 * @param ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @throws InvalidArgumentException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function updateLegalUnit(ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale) : int
	{
		$legalUnit = InseeSireneLegalUnit::findOne($uniteLegale->getSiren());
		if(null === $legalUnit)
		{
			$legalUnit = new InseeSireneLegalUnit();
			$legalUnit->insee_sirene_legal_unit_id = (int) $uniteLegale->getSiren();
		}
		
		$recordsToSave = [$legalUnit];
		$dateSince = $this->getDateSince($uniteLegale);
		
		if($uniteLegale->hasChangementCategorieJuridiqueUniteLegale())
		{
			$catjur = $this->handleLegalUnitHistoryCategorieJuridique(
				$legalUnit,
				$dateSince,
				$uniteLegale->getCategorieJuridiqueUniteLegale(),
			);
			if(null !== $catjur)
			{
				$recordsToSave[] = $catjur;
			}
		}
		
		if($uniteLegale->hasChangementCaractereEmployeurUniteLegale())
		{
			$employer = $this->handleLegalUnitHistoryCaractereEmployeur(
				$legalUnit,
				$dateSince,
				$uniteLegale->hasCaractereEmployeurUniteLegale(),
			);
			if(null !== $employer)
			{
				$recordsToSave[] = $employer;
			}
		}
		
		if($uniteLegale->hasChangementDenominationUsuelleUniteLegale())
		{
			$denomination = $this->handleLegalUnitHistoryDenomination(
				$legalUnit,
				$dateSince,
				$uniteLegale->getDenominationUsuelle1UniteLegale(),
				$uniteLegale->getDenominationUsuelle2UniteLegale(),
				$uniteLegale->getDenominationUsuelle3UniteLegale(),
			);
			if(null !== $denomination)
			{
				$recordsToSave[] = $denomination;
			}
		}
		
		if($uniteLegale->hasChangementEtatAdministratifUniteLegale())
		{
			$etat = $this->handleLegalUnitHistoryEtatAdministratif(
				$legalUnit,
				$dateSince,
				$uniteLegale->getEtatAdministratifUniteLegale(),
			);
			if(null !== $etat)
			{
				$recordsToSave[] = $etat;
			}
		}
		
		if($uniteLegale->hasChangementActivitePrincipaleUniteLegale())
		{
			$subclass = $this->handleLegalUnitHistoryNaf2008Lv5Subclass(
				$legalUnit,
				$dateSince,
				$uniteLegale->getActivitePrincipaleUniteLegale(),
				$uniteLegale->getNomenclatureActivitePrincipaleUniteLegale(),
			);
			if(null !== $subclass)
			{
				$recordsToSave[] = $subclass;
			}
		}
		
		if($uniteLegale->hasChangementEconomieSocialeSolidaireUniteLegale())
		{
			$ess = $this->handleLegalUnitHistorySocialEconomy(
				$legalUnit,
				$dateSince,
				$uniteLegale->hasEconomieSocialeSolidaireUniteLegale(),
			);
			if(null !== $ess)
			{
				$recordsToSave[] = $ess;
			}
		}
		
		if($uniteLegale->hasChangementSocieteMissionUniteLegale())
		{
			$smi = $this->handleLegalUnitHistorySocieteMission(
				$legalUnit,
				$dateSince,
				$uniteLegale->hasSocieteMissionUniteLegale(),
			);
			if(null !== $smi)
			{
				$recordsToSave[] = $smi;
			}
		}
		
		$count = 0;
		
		foreach($recordsToSave as $record)
		{
			$count += $this->saveRecord($record);
		}
		
		foreach($this->handleMoralPerson($legalUnit, $uniteLegale, $dateSince) as $record)
		{
			$count += $this->saveRecord($record);
		}
		
		foreach($this->handlePhysicalPerson($legalUnit, $uniteLegale, $dateSince) as $record)
		{
			$count += $this->saveRecord($record);
		}
		
		return $count;
	}
	
	/**
	 * Handles the saving of moral person if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale
	 * @param DateTimeInterface $dateSince
	 * @return array<integer, ActiveRecord>
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handleMoralPerson(
		InseeSireneLegalUnit $legalUnit,
		ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale,
		DateTimeInterface $dateSince
	) : array {
		
		if($uniteLegale->getDenominationUniteLegale() === null || $uniteLegale->getDenominationUniteLegale() === '')
		{
			return [];
		}
		
		if(!$uniteLegale->hasChangementDenominationUniteLegale())
		{
			return [];
		}
		
		$moralPerson = InseeSireneMoralPerson::findOne($legalUnit->insee_sirene_legal_unit_id);
		if(null === $moralPerson)
		{
			$moralPerson = new InseeSireneMoralPerson();
			$moralPerson->insee_sirene_legal_unit_id = $legalUnit->insee_sirene_legal_unit_id;
		}
		$recordsToSave = [$moralPerson];
		
		$raisonSociale = $this->handleMoralPersonHistoryRaisonSociale(
			$moralPerson,
			$dateSince,
			$uniteLegale->getDenominationUniteLegale(),
		);
		if(null !== $raisonSociale)
		{
			$recordsToSave[] = $raisonSociale;
		}
		
		return $recordsToSave;
	}
	
	/**
	 * Handles the saving of physicial person if needed.
	 * 
	 * @param InseeSireneLegalUnit $legalUnit
	 * @param ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale
	 * @param DateTimeInterface $dateSince
	 * @return array<integer, ActiveRecord>
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function handlePhysicalPerson(
		InseeSireneLegalUnit $legalUnit,
		ApiFrInseeSireneUniteLegaleHistoriqueInterface $uniteLegale,
		DateTimeInterface $dateSince
	) : array {
		
		if(($uniteLegale->getNomUniteLegale() === null || $uniteLegale->getNomUniteLegale() === '')
			&& ($uniteLegale->getNomUsageUniteLegale() === null || $uniteLegale->getNomUsageUniteLegale() === ''))
		{
			return [];
		}
		
		if(!$uniteLegale->hasChangementNomUniteLegale() && !$uniteLegale->hasChangementNomUsageUniteLegale())
		{
			return [];
		}
		
		$physicalPerson = InseeSirenePhysicalPerson::findOne($legalUnit->insee_sirene_legal_unit_id);
		if(null === $physicalPerson)
		{
			$physicalPerson = new InseeSirenePhysicalPerson();
			$physicalPerson->insee_sirene_legal_unit_id = $legalUnit->insee_sirene_legal_unit_id;
		}
		$recordsToSave = [$physicalPerson];
		
		$personHistory = $this->handlePhysicalPersonHistory(
			$physicalPerson,
			$dateSince,
			$uniteLegale->getNomUniteLegale(),
			$uniteLegale->getNomUsageUniteLegale(),
		);
		if(null !== $personHistory)
		{
			$recordsToSave[] = $personHistory;
		}
		
		return $recordsToSave;
	}
	
}
