<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratifInterface;
use RuntimeException;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;

/**
 * InseeSireneEtatAdministratifUpdater class file.
 * 
 * This class updates the InseeSireneEtatAdministratif records.
 * 
 * @author Anastaszor
 */
class InseeSireneEtatAdministratifUpdater extends InseeSireneRecordManager
{
	
	/**
	 * Updates all the sirene etats administratifs.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force
	 * @return integer the number of records saved
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Etats Administratifs');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_etat_administratif');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$count = 0;
		
		foreach($endpoint->getEtatAdministratifIterator() as $k => $etatAdministratif)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} Etats Administratifs', ['k' => (int) $k + 1]);
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			$count += $this->updateEtatAdministratif($etatAdministratif);
		}
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_etat_administratif';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$mdr->save();
		}
		
		return $count;
	}
	
	/**
	 * Updates the records from the given sirene etat administratif.
	 * 
	 * @param ApiFrInseeSireneEtatAdministratifInterface $etatAdministratif
	 * @return integer the number of records saved
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 */
	public function updateEtatAdministratif(ApiFrInseeSireneEtatAdministratifInterface $etatAdministratif) : int
	{
		$record = InseeSireneEtatAdministratif::findOne($etatAdministratif->getId());
		if(null === $record)
		{
			$record = new InseeSireneEtatAdministratif();
			$record->insee_sirene_etat_administratif_id = (int) $etatAdministratif->getId();
		}
		$record->code = (string) $etatAdministratif->getCode();
		$record->description = (string) $etatAdministratif->getName();
		if($record->save())
		{
			return 1;
		}
		
		$errors = [];
		
		foreach($record->getErrorSummary(true) as $error)
		{
			$errors[] = (string) $error;
		}
		
		$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
		$context = [
			'{class}' => \get_class($record),
			'{errs}' => \implode(',', $errors),
			'{obj}' => \json_encode($record->getAttributes(), \JSON_PRETTY_PRINT),
			'{old}' => \json_encode($record->getOldAttributes(), \JSON_PRETTY_PRINT),
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
}
