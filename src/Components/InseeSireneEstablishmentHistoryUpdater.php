<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissementHistorique;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishment;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryDenomination;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEmployer;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEnseigne;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;

/**
 * InseeSireneEstablishmentHistoryUpdater class file.
 * 
 * This class updates the InseeSireneEstablishment and derivated records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeSireneEstablishmentHistoryUpdater extends InseeSireneEstablishmentManager
{
	
	public const BATCH_SIZE = 500;
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The naf loader.
	 *
	 * @var InseeSireneNafLoader
	 */
	protected InseeSireneNafLoader $_nafLoader;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
		$this->_nafLoader = new InseeSireneNafLoader();
	}
	
	/**
	 * Gets the most recent datetime from etablissement history, or a default
	 * one if none.
	 * 
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @return DateTimeInterface
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getDateSince(ApiFrInseeSireneEtablissementHistorique $apiEtablissement) : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return $apiEtablissement->getDateDebut()
			?? DateTimeImmutable::createFromFormat('Y-m-d', '1900-01-01');
	}
	
	/**
	 * Updates all the sirene establishment histories.
	 * 
	 * @param ApiFrInseeSireneEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function updateAll(ApiFrInseeSireneEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null, bool $force = false) : int
	{
		$this->_logger->info('Processing Sirene Establishment Histories');
		
		$mdr = InseeSireneMetadata::findOne('insee_sirene_establishment_history');
		$dbdate = $endpoint->getLatestUploadDate();
		if(!$force && null !== $mdr)
		{
			if(!$this->isMoreRecentStr($mdr->contents, $dbdate))
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_sirene_establishment_history.'.$dbdate->format('Y-m');
		$mdc = InseeSireneMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeSireneMetadata();
			$mdc->insee_sirene_metadata_id = $mdcid;
			$mdc->contents = '0';
			$this->saveRecord($mdc);
		}
		
		$count = 0;
		$batchChangementEtatAdministratif = [];
		$batchChangementEnseigne = [];
		$batchChangementDenominationUsuelle = [];
		$batchChangementActivitePrincipale = [];
		$batchChangementCaractereEmployeur = [];
		
		/** @var ApiFrInseeSireneEtablissementHistorique $etablissementHistoric */
		foreach($endpoint->getLatestStockEtablissementHistoricIterator() as $k => $etablissementHistoric)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Establishment History Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Establishment History Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$this->saveRecord($mdc);
				$this->keepalive();
				
				$count += $this->updateEstablishmentHistoryEtatAdministratif($batchChangementEtatAdministratif);
				$batchChangementEtatAdministratif = [];
				$count += $this->updateEstablishmentHistoryEnseigne($batchChangementEnseigne);
				$batchChangementEnseigne = [];
				$count += $this->updateEstablishmentHistoryDenominationUsuelle($batchChangementDenominationUsuelle);
				$batchChangementDenominationUsuelle = [];
				$count += $this->updateEstablishmentHistoryActivitePrincipale($batchChangementActivitePrincipale);
				$batchChangementActivitePrincipale = [];
				$count += $this->updateEstablishmentHistoryCaractereEmployeur($batchChangementCaractereEmployeur);
				$batchChangementCaractereEmployeur = [];
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			if($etablissementHistoric->hasChangementEtatAdministratifEtablissement())
			{
				$batchChangementEtatAdministratif[] = $etablissementHistoric;
				if(\count($batchChangementEtatAdministratif) >= self::BATCH_SIZE)
				{
					$count += $this->updateEstablishmentHistoryEtatAdministratif($batchChangementEtatAdministratif);
					$batchChangementEtatAdministratif = [];
				}
			}
			
			if($etablissementHistoric->hasChangementEnseigneEtablissement())
			{
				$batchChangementEnseigne[] = $etablissementHistoric;
				if(\count($batchChangementEnseigne) >= self::BATCH_SIZE)
				{
					$count += $this->updateEstablishmentHistoryEnseigne($batchChangementEnseigne);
					$batchChangementEnseigne = [];
				}
			}
			
			if($etablissementHistoric->hasChangementDenominationUsuelleEtablissement())
			{
				$batchChangementDenominationUsuelle[] = $etablissementHistoric;
				if(\count($batchChangementDenominationUsuelle) >= self::BATCH_SIZE)
				{
					$count += $this->updateEstablishmentHistoryDenominationUsuelle($batchChangementDenominationUsuelle);
					$batchChangementDenominationUsuelle = [];
				}
			}
			
			if($etablissementHistoric->hasChangementActivitePrincipaleEtablissement())
			{
				$batchChangementActivitePrincipale[] = $etablissementHistoric;
				if(\count($batchChangementActivitePrincipale) >= self::BATCH_SIZE)
				{
					$count += $this->updateEstablishmentHistoryActivitePrincipale($batchChangementActivitePrincipale);
					$batchChangementActivitePrincipale = [];
				}
			}
			
			if($etablissementHistoric->hasChangementCaractereEmployeurEtablissement())
			{
				$batchChangementCaractereEmployeur[] = $etablissementHistoric;
				if(\count($batchChangementCaractereEmployeur) >= self::BATCH_SIZE)
				{
					$count += $this->updateEstablishmentHistoryCaractereEmployeur($batchChangementCaractereEmployeur);
					$batchChangementCaractereEmployeur = [];
				}
			}
		}
		
		$count += $this->updateEstablishmentHistoryEtatAdministratif($batchChangementEtatAdministratif);
		$count += $this->updateEstablishmentHistoryEnseigne($batchChangementEnseigne);
		$count += $this->updateEstablishmentHistoryDenominationUsuelle($batchChangementDenominationUsuelle);
		$count += $this->updateEstablishmentHistoryActivitePrincipale($batchChangementActivitePrincipale);
		$count += $this->updateEstablishmentHistoryCaractereEmployeur($batchChangementCaractereEmployeur);
		
		if(null === $mdr)
		{
			$mdr = new InseeSireneMetadata();
			$mdr->insee_sirene_metadata_id = 'insee_sirene_establishment_history';
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdr->contents);
		if(empty($dti) || $dti->getTimestamp() < $dbdate->getTimestamp())
		{
			$mdr->contents = $dbdate->format('Y-m-d');
			$this->saveRecord($mdr);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment history 
	 * etat administratif changes.
	 * 
	 * @param array<integer, ApiFrInseeSireneEtablissementHistorique> $batch
	 * @return integer the number of records updated
	 */
	public function updateEstablishmentHistoryEtatAdministratif(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		$pk2 = [];
		
		/** @var ApiFrInseeSireneEtablissementHistorique $etablissementHistoryLine */
		foreach($batch as $etablissementHistoryLine)
		{
			$pks[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
			];
			$pk2[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
				'date_since' => $this->getDateSince($etablissementHistoryLine)->format('Y-m-d'),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneEstablishment> $etablissements */
		$etablissements = InseeSireneEstablishment::findAll($pks);
		/** @var array<integer, InseeSireneEstablishmentHistoryEtatAdministratif> $etatAdministratifs */
		$etatAdministratifs = InseeSireneEstablishmentHistoryEtatAdministratif::findAll($pk2);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, InseeSireneEstablishment> $orderedEstablishments */
		$orderedEstablishments = [];
		/** @var array<integer, array<string, InseeSireneEstablishmentHistoryEtatAdministratif>> $orderedEtatAdministratifs */
		$orderedEtatAdministratifs = [];
		
		foreach($etablissements as $record)
		{
			$orderedEstablishments[(int) $record->insee_sirene_establishment_id] = $record;
		}
		
		foreach($etatAdministratifs as $record)
		{
			$orderedEtatAdministratifs[(int) $record->insee_sirene_establishment_id][(string) $record->date_since] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $etablissementHistoryLine)
		{
			try
			{
				$count += $this->updateEstablishmentEtatAdministratif(
					$etablissementHistoryLine,
					$orderedEstablishments[(int) $etablissementHistoryLine->getSiret()] ?? null,
					$orderedEtatAdministratifs[(int) $etablissementHistoryLine->getSiret()][(string) $this->getDateSince($etablissementHistoryLine)->format('Y-m-d')] ?? null,
				);
			}
			catch(RuntimeException $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment history
	 * enseigne changes.
	 * 
	 * @param array<integer, ApiFrInseeSireneEtablissementHistorique> $batch
	 * @return integer the number of records updated
	 */
	public function updateEstablishmentHistoryEnseigne(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		$pk2 = [];
		
		/** @var ApiFrInseeSireneEtablissementHistorique $etablissementHistoryLine */
		foreach($batch as $etablissementHistoryLine)
		{
			$pks[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
			];
			$pk2[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
				'date_since' => $this->getDateSince($etablissementHistoryLine)->format('Y-m-d'),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneEstablishment> $etablissements */
		$etablissements = InseeSireneEstablishment::findAll($pks);
		/** @var array<integer, InseeSireneEstablishmentHistoryEnseigne> $enseignes */
		$enseignes = InseeSireneEstablishmentHistoryEnseigne::findAll($pk2);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, InseeSireneEstablishment> $orderedEstablishments */
		$orderedEstablishments = [];
		/** @var array<integer, array<string, InseeSireneEstablishmentHistoryEnseigne>> $orderedEnseignes */
		$orderedEnseignes = [];
		
		foreach($etablissements as $record)
		{
			$orderedEstablishments[(int) $record->insee_sirene_establishment_id] = $record;
		}
		
		foreach($enseignes as $record)
		{
			$orderedEnseignes[(int) $record->insee_sirene_establishment_id][(string) $record->date_since] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $etablissementHistoryLine)
		{
			try
			{
				$count += $this->updateEstablishmentEnseigne(
					$etablissementHistoryLine,
					$orderedEstablishments[(int) $etablissementHistoryLine->getSiret()] ?? null,
					$orderedEnseignes[(int) $etablissementHistoryLine->getSiret()][(string) $this->getDateSince($etablissementHistoryLine)->format('Y-m-d')] ?? null,
				);
			}
			catch(RuntimeException $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment history
	 * denomination usuelle changes.
	 * 
	 * @param array<integer, ApiFrInseeSireneEtablissementHistorique> $batch
	 * @return integer the number of records updated
	 */
	public function updateEstablishmentHistoryDenominationUsuelle(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		$pk2 = [];
		
		/** @var ApiFrInseeSireneEtablissementHistorique $etablissementHistoryLine */
		foreach($batch as $etablissementHistoryLine)
		{
			$pks[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
			];
			$pk2[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
				'date_since' => $this->getDateSince($etablissementHistoryLine)->format('Y-m-d'),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneEstablishment> $etablissements */
		$etablissements = InseeSireneEstablishment::findAll($pks);
		/** @var array<integer, InseeSireneEstablishmentHistoryDenomination> $denominations */
		$denominations = InseeSireneEstablishmentHistoryDenomination::findAll($pk2);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, InseeSireneEstablishment> $orderedEstablishments */
		$orderedEstablishments = [];
		/** @var array<integer, array<string, InseeSireneEstablishmentHistoryDenomination>> $orderedDenominations */
		$orderedDenominations = [];
		
		foreach($etablissements as $record)
		{
			$orderedEstablishments[(int) $record->insee_sirene_establishment_id] = $record;
		}
		
		foreach($denominations as $record)
		{
			$orderedDenominations[(int) $record->insee_sirene_establishment_id][(string) $record->date_since] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $etablissementHistoryLine)
		{
			try
			{
				$count += $this->updateEstablishmentDenominationUsuelle(
					$etablissementHistoryLine,
					$orderedEstablishments[(int) $etablissementHistoryLine->getSiret()] ?? null,
					$orderedDenominations[(int) $etablissementHistoryLine->getSiret()][(string) $this->getDateSince($etablissementHistoryLine)->format('Y-m-d')] ?? null,
				);
			}
			catch(RuntimeException $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment history
	 * activite principale changes.
	 * 
	 * @param array<integer, ApiFrInseeSireneEtablissementHistorique> $batch
	 * @return integer the number of records updated
	 */
	public function updateEstablishmentHistoryActivitePrincipale(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		$pk2 = [];
		
		/** @var ApiFrInseeSireneEtablissementHistorique $etablissementHistoryLine */
		foreach($batch as $etablissementHistoryLine)
		{
			$pks[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
			];
			$pk2[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
				'date_since' => $this->getDateSince($etablissementHistoryLine)->format('Y-m-d'),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneEstablishment> $etablissements */
		$etablissements = InseeSireneEstablishment::findAll($pks);
		/** @var array<integer, InseeSireneEstablishmentHistoryNaf2008Lv5Subclass> $activites */
		$activites = InseeSireneEstablishmentHistoryNaf2008Lv5Subclass::findAll($pk2);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, InseeSireneEstablishment> $orderedEstablishments */
		$orderedEstablishments = [];
		/** @var array<integer, array<string, InseeSireneEstablishmentHistoryNaf2008Lv5Subclass>> $orderedActivites */
		$orderedActivites = [];
		
		foreach($etablissements as $record)
		{
			$orderedEstablishments[(int) $record->insee_sirene_establishment_id] = $record;
		}
		
		foreach($activites as $record)
		{
			$orderedActivites[(int) $record->insee_sirene_establishment_id][(string) $record->date_since] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $etablissementHistoryLine)
		{
			try
			{
				$count += $this->updateEstablishmentActivitePrincipale(
					$etablissementHistoryLine,
					$orderedEstablishments[(int) $etablissementHistoryLine->getSiret()] ?? null,
					$orderedActivites[(int) $etablissementHistoryLine->getSiret()][(string) $this->getDateSince($etablissementHistoryLine)->format('Y-m-d')] ?? null,
				);
			}
			catch(RuntimeException $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the records for the given sirene establishment history
	 * caractere employeur changes.
	 * 
	 * @param array<integer, ApiFrInseeSireneEtablissementHistorique> $batch
	 * @return integer the number of records updated
	 */
	public function updateEstablishmentHistoryCaractereEmployeur(array $batch) : int
	{
		// {{{ step 1 : forge the pks for the big select
		$pks = [];
		$pk2 = [];
		
		/** @var ApiFrInseeSireneEtablissementHistorique $etablissementHistoryLine */
		foreach($batch as $etablissementHistoryLine)
		{
			$pks[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
			];
			$pk2[] = [
				'insee_sirene_establishment_id' => $etablissementHistoryLine->getSiret(),
				'date_since' => $this->getDateSince($etablissementHistoryLine)->format('Y-m-d'),
			];
		}
		// }}} step 1
		
		/** @var array<integer, InseeSireneEstablishment> $etablissements */
		$etablissements = InseeSireneEstablishment::findAll($pks);
		/** @var array<integer, InseeSireneEstablishmentHistoryEmployer> $employeurs */
		$employeurs = InseeSireneEstablishmentHistoryEmployer::findAll($pk2);
		
		// {{{ step 2 : index the retrieved records for them to be used
		/** @var array<integer, InseeSireneEstablishment> $orderedEstablishments */
		$orderedEstablishments = [];
		/** @var array<integer, array<string, InseeSireneEstablishmentHistoryEmployer>> $orderedEmployeurs */
		$orderedEmployeurs = [];
		
		foreach($etablissements as $record)
		{
			$orderedEstablishments[(int) $record->insee_sirene_establishment_id] = $record;
		}
		
		foreach($employeurs as $record)
		{
			$orderedEmployeurs[(int) $record->insee_sirene_establishment_id][(string) $record->date_since] = $record;
		}
		// }}} step 2
		
		$count = 0;
		
		foreach($batch as $etablissementHistoryLine)
		{
			try
			{
				$count += $this->updateEstablishmentCaractereEmployeur(
					$etablissementHistoryLine,
					$orderedEstablishments[(int) $etablissementHistoryLine->getSiret()] ?? null,
					$orderedEmployeurs[(int) $etablissementHistoryLine->getSiret()][(string) $this->getDateSince($etablissementHistoryLine)->format('Y-m-d')] ?? null,
				);
			}
			catch(RuntimeException $exc)
			{
				$message = [];
				$throwable = $exc;
				
				while(null !== $throwable)
				{
					$message[] = $throwable->getMessage();
					$message[] = $throwable->getTraceAsString();
					$throwable = $throwable->getPrevious();
				}
				
				$this->_logger->warning(\implode("\n\n", $message));
			}
		}
		
		return $count;
	}
	
	
	/**
	 * Updates the etat administratif for the given establishment history.
	 * 
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @param InseeSireneEstablishment $establishment
	 * @param InseeSireneEstablishmentHistoryEtatAdministratif $etatAdministratif
	 * @return integer the number of records saved
	 * @throws RuntimeException
	 */
	public function updateEstablishmentEtatAdministratif(
		ApiFrInseeSireneEtablissementHistorique $apiEtablissement,
		?InseeSireneEstablishment $establishment,
		?InseeSireneEstablishmentHistoryEtatAdministratif $etatAdministratif
	) : int {
		$establishment = $this->ensureEstablishment($establishment, $apiEtablissement);
		
		$mustBeSaved = false;
		$dateSince = $this->getDateSince($apiEtablissement);
		
		if(null === $etatAdministratif)
		{
			$etatAdministratif = new InseeSireneEstablishmentHistoryEtatAdministratif();
			$etatAdministratif->insee_sirene_establishment_id = (int) $establishment->insee_sirene_establishment_id;
			$etatAdministratif->date_since = $dateSince->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$etat = $apiEtablissement->getEtatAdministratifEtablissement();
		if(null !== $etat)
		{
			if($etat->getId() !== $etatAdministratif->insee_sirene_etat_administratif_id)
			{
				$etatAdministratif->insee_sirene_etat_administratif_id = (int) $etat->getId();
				$mustBeSaved = true;
			}
		}
		
		return (int) ($mustBeSaved || (bool) $this->saveRecord($etatAdministratif));
	}
	
	/**
	 * Updates the enseigne for the given establishment history.
	 * 
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @param ?InseeSireneEstablishment $establishment
	 * @param ?InseeSireneEstablishmentHistoryEnseigne $enseigne
	 * @return integer the number of records saved
	 * @throws RuntimeException
	 */
	public function updateEstablishmentEnseigne(
		ApiFrInseeSireneEtablissementHistorique $apiEtablissement,
		?InseeSireneEstablishment $establishment,
		?InseeSireneEstablishmentHistoryEnseigne $enseigne
	) : int {
		$establishment = $this->ensureEstablishment($establishment, $apiEtablissement);
		
		$mustBeSaved = false;
		$dateSince = $this->getDateSince($apiEtablissement);
		
		if(null === $enseigne)
		{
			$enseigne = new InseeSireneEstablishmentHistoryEnseigne();
			$enseigne->insee_sirene_establishment_id = (int) $establishment->insee_sirene_establishment_id;
			$enseigne->date_since = $dateSince->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$enseigne1 = $apiEtablissement->getEnseigne1Etablissement();
		if(null !== $enseigne1 && '' !== $enseigne1)
		{
			if($enseigne->enseigne_1 !== $enseigne1)
			{
				$enseigne->enseigne_1 = $enseigne1;
				$mustBeSaved = true;
			}
		}
		
		$enseigne2 = $apiEtablissement->getEnseigne2Etablissement();
		if(null !== $enseigne2 && '' !== $enseigne2)
		{
			if($enseigne->enseigne_2 !== $enseigne2)
			{
				$enseigne->enseigne_2 = $enseigne2;
				$mustBeSaved = true;
			}
		}
		
		$enseigne3 = $apiEtablissement->getEnseigne3Etablissement();
		if(null !== $enseigne3 && '' !== $enseigne3)
		{
			if($enseigne->enseigne_3 !== $enseigne3)
			{
				$enseigne->enseigne_3 = $enseigne3;
				$mustBeSaved = true;
			}
		}
		
		return (int) ($mustBeSaved || (bool) $this->saveRecord($enseigne));
	}
	
	/**
	 * Updates the denomination for the given establishment history.
	 * 
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @param ?InseeSireneEstablishment $establishment
	 * @param ?InseeSireneEstablishmentHistoryDenomination $denomination
	 * @return integer the number of records saved
	 * @throws RuntimeException
	 */
	public function updateEstablishmentDenominationUsuelle(
		ApiFrInseeSireneEtablissementHistorique $apiEtablissement,
		?InseeSireneEstablishment $establishment,
		?InseeSireneEstablishmentHistoryDenomination $denomination
	) : int {
		$establishment = $this->ensureEstablishment($establishment, $apiEtablissement);
		
		$mustBeSaved = false;
		$dateSince = $this->getDateSince($apiEtablissement);
		
		if(null === $denomination)
		{
			$denomination = new InseeSireneEstablishmentHistoryDenomination();
			$denomination->insee_sirene_establishment_id = (int) $establishment->insee_sirene_establishment_id;
			$denomination->date_since = $dateSince->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$usage = $apiEtablissement->getDenominationUsuelleEtablissement();
		if(null !== $usage && '' !== $usage)
		{
			if($denomination->usage_denomination !== $usage)
			{
				$denomination->usage_denomination = $usage;
				$mustBeSaved = true;
			}
		}
		
		return (int) ($mustBeSaved || (bool) $this->saveRecord($denomination));
	}
	
	/**
	 * Updates the activite principale for the given establishment history.
	 * 
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @param ?InseeSireneEstablishment $establishment
	 * @param ?InseeSireneEstablishmentHistoryNaf2008Lv5Subclass $subclass
	 * @return integer the number of records saved
	 * @throws RuntimeException
	 */
	public function updateEstablishmentActivitePrincipale(
		ApiFrInseeSireneEtablissementHistorique $apiEtablissement,
		?InseeSireneEstablishment $establishment,
		?InseeSireneEstablishmentHistoryNaf2008Lv5Subclass $subclass
	) : int {
		$establishment = $this->ensureEstablishment($establishment, $apiEtablissement);
		
		$mustBeSaved = false;
		$dateSince = $this->getDateSince($apiEtablissement);
		
		if(null === $subclass)
		{
			$subclass = new InseeSireneEstablishmentHistoryNaf2008Lv5Subclass();
			$subclass->insee_sirene_establishment_id = (int) $establishment->insee_sirene_establishment_id;
			$subclass->date_since = $dateSince->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$activitePrincipale = $apiEtablissement->getActivitePrincipaleEtablissement();
		$nomenclature = $apiEtablissement->getNomenclatureActivitePrincipaleEtablissement();
		if(null !== $activitePrincipale && '' !== $activitePrincipale)
		{
			try
			{
				$activiteId = $this->_nafLoader->getNaf2008Lv5SubclassId($activitePrincipale, $nomenclature);
			}
			catch(InvalidArgumentException $exc)
			{
				$message = 'Failed to get naf2008 subclass with id {aid} and nomenclature {nom} : {exc}';
				$context = ['aid' => $activitePrincipale, 'nom' => $nomenclature, 'exc' => $exc->getMessage()];
				$this->_logger->warning($message, $context);
				$activiteId = null;
			}
			
			if(null !== $activiteId && $subclass->insee_naf2008_lv5_subclass_id !== $activiteId)
			{
				$subclass->insee_naf2008_lv5_subclass_id = $activiteId;
				$mustBeSaved = true;
			}
		}
		
		return (int) ($mustBeSaved || (bool) $this->saveRecord($subclass));
	}
	
	/**
	 * Updates the caractere employeur for the given establishment history.
	 * 
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @param ?InseeSireneEstablishment $establishment
	 * @param ?InseeSireneEstablishmentHistoryEmployer $caractereEmployeur
	 * @return integer the number of records saved
	 * @throws RuntimeException
	 */
	public function updateEstablishmentCaractereEmployeur(
		ApiFrInseeSireneEtablissementHistorique $apiEtablissement,
		?InseeSireneEstablishment $establishment,
		?InseeSireneEstablishmentHistoryEmployer $caractereEmployeur
	) : int {
		
		$establishment = $this->ensureEstablishment($establishment, $apiEtablissement);
		
		$mustBeSaved = false;
		$dateSince = $this->getDateSince($apiEtablissement);
		
		if(null === $caractereEmployeur)
		{
			$caractereEmployeur = new InseeSireneEstablishmentHistoryEmployer();
			$caractereEmployeur->insee_sirene_establishment_id = (int) $establishment->insee_sirene_establishment_id;
			$caractereEmployeur->date_since = $dateSince->format('Y-m-d');
			$mustBeSaved = true;
		}
		
		$isEmployeur = $apiEtablissement->hasCaractereEmployeurEtablissement();
		if(null !== $isEmployeur)
		{
			if((int) $isEmployeur !== $caractereEmployeur->is_employer)
			{
				$caractereEmployeur->is_employer = (int) $isEmployeur;
				$mustBeSaved = true;
			}
		}
		
		return (int) ($mustBeSaved || (bool) $this->saveRecord($caractereEmployeur));
	}
	
	/**
	 * Ensures that there is a saved establishment.
	 * 
	 * @param ?InseeSireneEstablishment $record
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @return InseeSireneEstablishment
	 * @throws RuntimeException
	 */
	public function ensureEstablishment(?InseeSireneEstablishment $record, ApiFrInseeSireneEtablissementHistorique $apiEtablissement) : InseeSireneEstablishment
	{
		if(null === $record)
		{
			$record = new InseeSireneEstablishment();
			$record->insee_sirene_establishment_id = (int) $apiEtablissement->getSiret();
			$record->insee_sirene_legal_unit_id = (int) $apiEtablissement->getSiren();
			$record->insee_cog_pays_id = 99100; // metropolitan france by default
			$record->insee_ban_address_fitness = 0; // no fitness by default
			$this->saveRecord($record);
		}
		
		return $record;
	}
	
	
	/**
	 * Updates all the records for the given etablissement.
	 * 
	 * @param ApiFrInseeSireneEtablissementHistorique $apiEtablissement
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @deprecated
	 */
	public function updateEstablishment(ApiFrInseeSireneEtablissementHistorique $apiEtablissement) : int
	{
		$record = InseeSireneEstablishment::findOne($apiEtablissement->getSiret());
		if(null === $record)
		{
			$record = new InseeSireneEstablishment();
			$record->insee_sirene_establishment_id = (int) $apiEtablissement->getSiret();
			$record->insee_sirene_legal_unit_id = (int) $apiEtablissement->getSiren();
			$record->insee_cog_pays_id = 99100; // metropolitan france by default
			$record->insee_ban_address_fitness = 0; // no fitness by default
		}
		$recordsToSave = [$record];
		
		$dateSince = $this->getDateSince($apiEtablissement);
		
		if($apiEtablissement->hasChangementDenominationUsuelleEtablissement())
		{
			$denomination = $this->handleEstablishmentHistoryDenomination(
				$record,
				$dateSince,
				$apiEtablissement->getDenominationUsuelleEtablissement(),
			);
			if(null !== $denomination)
			{
				$recordsToSave[] = $denomination;
			}
		}
		
		if($apiEtablissement->hasChangementCaractereEmployeurEtablissement())
		{
			$employer = $this->handleEstablishmentHistoryCaractereEmployeur(
				$record,
				$dateSince,
				$apiEtablissement->hasCaractereEmployeurEtablissement(),
			);
			if(null !== $employer)
			{
				$recordsToSave[] = $employer;
			}
		}
		
		if($apiEtablissement->hasChangementEnseigneEtablissement())
		{
			$enseigne = $this->handleEstablishmentEnseigne(
				$record,
				$dateSince,
				$apiEtablissement->getEnseigne1Etablissement(),
				$apiEtablissement->getEnseigne2Etablissement(),
				$apiEtablissement->getEnseigne3Etablissement(),
			);
			if(null !== $enseigne)
			{
				$recordsToSave[] = $enseigne;
			}
		}
		
		if($apiEtablissement->hasChangementEtatAdministratifEtablissement())
		{
			$etat = $this->handleEstablishmentEtatAdministratif(
				$record,
				$dateSince,
				$apiEtablissement->getEtatAdministratifEtablissement(),
			);
			if(null !== $etat)
			{
				$recordsToSave[] = $etat;
			}
		}
		
		if($apiEtablissement->hasChangementActivitePrincipaleEtablissement())
		{
			$activite = $this->handleEstablishmentNaf2008Lv5Subclass(
				$record,
				$dateSince,
				$apiEtablissement->getActivitePrincipaleEtablissement(),
				$apiEtablissement->getNomenclatureActivitePrincipaleEtablissement(),
			);
			if(null !== $activite)
			{
				$recordsToSave[] = $activite;
			}
		}
		
		$count = 0;
		
		foreach($recordsToSave as $record)
		{
			$count += $this->saveRecord($record);
		}
		
		return $count;
	}
	
}
