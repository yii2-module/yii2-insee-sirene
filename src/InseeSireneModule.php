<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneCategorieEntreprise;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishment;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentAddress;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryDenomination;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEmployer;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEnseigne;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryNaf2008Lv6Artisanat;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistorySiege;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEstablishmentHistoryTrancheEffectif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnit;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryCategorieEntreprise;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryCatjurN3;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryDenomination;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryEmployer;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryEtatAdministratif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistorySocialEconomy;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneLegalUnitHistoryTrancheEffectif;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMetadata;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPerson;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPersonHistoryRaisonSociale;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneMoralPersonHistorySigle;
use Yii2Module\Yii2InseeSirene\Models\InseeSirenePhysicalPerson;
use Yii2Module\Yii2InseeSirene\Models\InseeSirenePhysicalPersonHistory;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneSuccession;
use Yii2Module\Yii2InseeSirene\Models\InseeSireneTrancheEffectif;

/**
 * InseeSireneModule class file.
 * 
 * This module is to represent the data of entreprises in french jurisdictions
 * with consolidated informations, in order to be able to build other resources
 * that uses those standardized economic features.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeSireneModule extends BootstrappedModule
{
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'building';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('InseeSireneModule.Module', 'Sirene');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'unit' => new Bundle(BaseYii::t('InseeSireneModule.Module', 'Legal Unit'), [
				'unit' => (new Record(InseeSireneLegalUnit::class, 'unit', BaseYii::t('InseeSireneModule.Module', 'Legal Unit')))->enableFullAccess(),
				'hist-catent' => (new Record(InseeSireneLegalUnitHistoryCategorieEntreprise::class, 'hist-catent', BaseYii::t('InseeSireneModule.Module', 'History Categorie')))->enableFullAccess(),
				'hist-catjur' => (new Record(InseeSireneLegalUnitHistoryCatjurN3::class, 'hist-catjur', BaseYii::t('InseeSireneModule.Module', 'History Cat Jur')))->enableFullAccess(),
				'hist-denomi' => (new Record(InseeSireneLegalUnitHistoryDenomination::class, 'hist-denomi', BaseYii::t('InseeSireneModule.Module', 'History Denomination')))->enableFullAccess(),
				'hist-employ' => (new Record(InseeSireneLegalUnitHistoryEmployer::class, 'hist-employ', BaseYii::t('InseeSireneModule.Module', 'History Employer')))->enableFullAccess(),
				'hist-etatdm' => (new Record(InseeSireneLegalUnitHistoryEtatAdministratif::class, 'hist-etatdm', BaseYii::t('InseeSireneModule.Module', 'History Etat Administratif')))->enableFullAccess(),
				'hist-naflv5' => (new Record(InseeSireneLegalUnitHistoryNaf2008Lv5Subclass::class, 'hist-naflv5', BaseYii::t('InseeSireneModule.Module', 'History Naf Subclass')))->enableFullAccess(),
				'hist-social' => (new Record(InseeSireneLegalUnitHistorySocialEconomy::class, 'hist-social', BaseYii::t('InseeSireneModule.Module', 'History Social')))->enableFullAccess(),
				'hist-tranch' => (new Record(InseeSireneLegalUnitHistoryTrancheEffectif::class, 'hist-tranch', BaseYii::t('InseeSireneModule.Module', 'History Tranche')))->enableFullAccess(),
				'moral' => (new Record(InseeSireneMoralPerson::class, 'moral', BaseYii::t('InseeSireneModule.Module', 'Moral Person')))->enableFullAccess(),
				'hist-raisoc' => (new Record(InseeSireneMoralPersonHistoryRaisonSociale::class, 'hist-raisoc', BaseYii::t('InseeSireneModule.Module', 'History Raison Sociale')))->enableFullAccess(),
				'hist-sigle' => (new Record(InseeSireneMoralPersonHistorySigle::class, 'hist-sigle', BaseYii::t('InseeSireneModule.Module', 'History Sigle')))->enableFullAccess(),
				'physical' => (new Record(InseeSirenePhysicalPerson::class, 'physical', BaseYii::t('InseeSireneModule.Module', 'Physical Person')))->enableFullAccess(),
				'hist-physical' => (new Record(InseeSirenePhysicalPersonHistory::class, 'hist-physical', BaseYii::t('InseeSireneModule.Module', 'History Physical')))->enableFullAccess(),
			]),
			'etab' => new Bundle(BaseYii::t('InseeSireneModule.Module', 'Etablissement'), [
				'etab' => (new Record(InseeSireneEstablishment::class, 'etab', BaseYii::t('InseeSireneModule.Module', 'Etablishment')))->enableFullAccess(),
				'etab-addres' => (new Record(InseeSireneEstablishmentAddress::class, 'etab-addres', BaseYii::t('InseeSireneModule.Module', 'Etablishment Address')))->enableFullAccess(),
				'hist-denomi' => (new Record(InseeSireneEstablishmentHistoryDenomination::class, 'hist-denomi', BaseYii::t('InseeSireneModule.Module', 'History Denomination')))->enableFullAccess(),
				'hist-employ' => (new Record(InseeSireneEstablishmentHistoryEmployer::class, 'hist-employ', BaseYii::t('InseeSireneModule.Module', 'History Employer')))->enableFullAccess(),
				'hist-enseig' => (new Record(InseeSireneEstablishmentHistoryEnseigne::class, 'hist-eneign', BaseYii::t('InseeSireneModule.Module', 'History Enseigne')))->enableFullAccess(),
				'hist-etatdm' => (new Record(InseeSireneEstablishmentHistoryEtatAdministratif::class, 'hist-etatdm', BaseYii::t('InseeSireneModule.Module', 'History Etat')))->enableFullAccess(),
				'hist-naflv5' => (new Record(InseeSireneEstablishmentHistoryNaf2008Lv5Subclass::class, 'hist-naflv5', BaseYii::t('InseeSireneModule.Module', 'History Naf Subclass')))->enableFullAccess(),
				'hist-naflv6' => (new Record(InseeSireneEstablishmentHistoryNaf2008Lv6Artisanat::class, 'hist-naflv6', BaseYii::t('InseeSireneModule.Module', 'History Naf Artisanat')))->enableFullAccess(),
				'hist-siege' => (new Record(InseeSireneEstablishmentHistorySiege::class, 'hist-siege', BaseYii::t('InseeSireneModule.Module', 'History Siege')))->enableFullAccess(),
				'hist-tranch' => (new Record(InseeSireneEstablishmentHistoryTrancheEffectif::class, 'hist-tranch', BaseYii::t('InseeSireneModule.Module', 'History Tranche')))->enableFullAccess(),
				'succession' => (new Record(InseeSireneSuccession::class, 'succession', BaseYii::t('InseeSireneModule.Module', 'Succession')))->enableFullAccess(),
			]),
			'ref' => new Bundle(BaseYii::t('InseeSireneModule.Module', 'Referentiel'), [
				'cat' => (new Record(InseeSireneCategorieEntreprise::class, 'cat', BaseYii::t('InseeSireneModule.Module', 'Categorie Entreprise')))->enableFullAccess(),
				'etat' => (new Record(InseeSireneEtatAdministratif::class, 'etat', BaseYii::t('InseeSireneModule.Module', 'Etat Administratif')))->enableFullAccess(),
				'metadata' => (new Record(InseeSireneMetadata::class, 'metadata', BaseYii::t('InseeSireneModule.Module', 'Metadata')))->enableFullAccess(),
				'tranche' => (new Record(InseeSireneTrancheEffectif::class, 'tranche', BaseYii::t('InseeSireneModule.Module', 'Tranche Effectif')))->enableFullAccess(),
			]),
		];
	}
	
}
