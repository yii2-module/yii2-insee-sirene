<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Models;

use yii\base\InvalidConfigException;
use yii\base\UnknownMethodException;
use yii\BaseYii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "insee_sirene_establishment_address".
 *
 * @property integer $insee_sirene_establishment_id The siret number of the establishment at this address (+/-1)
 * @property ?string $insee_ban_address_id The id of the related ban address
 * @property integer $insee_ban_address_fitness The fitness index of the related ban address (0 = nul, 255 = max)
 * @property ?string $insee_ban_address_raw The raw data of the ban address
 * @property ?string $complement_address The complement of the address
 * @property ?string $libelle_foreign_city The libelle of the foreign city if the address is not french
 * @property ?string $special_distribution The special distribution for the address
 * @property ?string $cedex_code The code of the cedex, if any
 * @property ?string $cedex_label The label of the cedex, if any
 *
 * @property InseeSireneEstablishment $inseeSireneEstablishment
 *
 * /!\ WARNING /!\
 * This class is generated by the gii module, please do not edit manually this
 * file as the changes may be overritten.
 *
 * @author Anastaszor
 */
class InseeSireneEstablishmentAddress extends ActiveRecord
{

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::tableName()
	 */
	public static function tableName() : string
	{
		return 'insee_sirene_establishment_address';
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::getDb()
	 * @return Connection the database connection used by this AR class
	 * @throws InvalidConfigException
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public static function getDb() : Connection
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement, NullableReturnStatement */
		return BaseYii::$app->get('db_insee_sirene');
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, boolean|integer|float|string|array<integer|string, boolean|integer|float|string>>>
	 */
	public function rules() : array
	{
		return [
			[['insee_sirene_establishment_id'], 'required'],
			[['insee_sirene_establishment_id', 'insee_ban_address_fitness'], 'integer'],
			[['insee_ban_address_id'], 'string', 'max' => 22],
			[['insee_ban_address_raw'], 'string', 'max' => 255],
			[['complement_address'], 'string', 'max' => 38],
			[['libelle_foreign_city', 'cedex_label'], 'string', 'max' => 100],
			[['special_distribution'], 'string', 'max' => 26],
			[['cedex_code'], 'string', 'max' => 9],
		];
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::attributeLabels()
	 * @return array<string, string>
	 */
	public function attributeLabels() : array
	{
		return [
			'insee_sirene_establishment_id' => BaseYii::t('InseeSireneModule.Models', 'Insee Sirene Establishment ID'),
			'insee_ban_address_id' => BaseYii::t('InseeSireneModule.Models', 'Insee Ban Address ID'),
			'insee_ban_address_fitness' => BaseYii::t('InseeSireneModule.Models', 'Insee Ban Address Fitness'),
			'insee_ban_address_raw' => BaseYii::t('InseeSireneModule.Models', 'Insee Ban Address Raw'),
			'complement_address' => BaseYii::t('InseeSireneModule.Models', 'Complement Address'),
			'libelle_foreign_city' => BaseYii::t('InseeSireneModule.Models', 'Libelle Foreign City'),
			'special_distribution' => BaseYii::t('InseeSireneModule.Models', 'Special Distribution'),
			'cedex_code' => BaseYii::t('InseeSireneModule.Models', 'Cedex Code'),
			'cedex_label' => BaseYii::t('InseeSireneModule.Models', 'Cedex Label'),
		];
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getInseeSireneEstablishment() : ActiveQuery
	{
		return $this->hasOne(InseeSireneEstablishment::class, ['insee_sirene_establishment_id' => 'insee_sirene_establishment_id']);
	}

}
