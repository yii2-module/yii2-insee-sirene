/**
 * Database relations required by InseeSireneModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-sirene
 * @license MIT
 */

ALTER TABLE `insee_sirene_legal_unit` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_catjur_n3`;
ALTER TABLE `insee_sirene_legal_unit` ADD CONSTRAINT `fk_insee_sirene_legal_unit_catjur_n3` FOREIGN KEY (`insee_catjur_n3_id`) REFERENCES `insee_catjur`.`insee_catjur_n3`(`insee_catjur_n3_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_naf2008_lv5_subclass`;
ALTER TABLE `insee_sirene_legal_unit` ADD CONSTRAINT `fk_insee_sirene_legal_unit_naf2008_lv5_subclass` FOREIGN KEY (`insee_naf2008_lv5_subclass_id`) REFERENCES `insee_naf`.`insee_naf2008_lv5_subclass`(`insee_naf2008_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_category_entreprise`;
ALTER TABLE `insee_sirene_legal_unit` ADD CONSTRAINT `fk_insee_sirene_legal_unit_category_entreprise` FOREIGN KEY (`insee_sirene_category_entreprise_id`) REFERENCES `insee_sirene_categorie_entreprise`(`insee_sirene_categorie_entreprise_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_etat_administratif`;
ALTER TABLE `insee_sirene_legal_unit` ADD CONSTRAINT `fk_insee_sirene_legal_unit_etat_administratif` FOREIGN KEY (`insee_sirene_etat_administratif_id`) REFERENCES `insee_sirene_etat_administratif`(`insee_sirene_etat_administratif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_tranche_effectif`;
ALTER TABLE `insee_sirene_legal_unit` ADD CONSTRAINT `fk_insee_sirene_legal_unit_tranche_effectif` FOREIGN KEY (`insee_sirene_tranche_effectif_id`) REFERENCES `insee_sirene_tranche_effectif`(`insee_sirene_tranche_effectif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_categorie_entreprise` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_categorie_entreprise_1`;
ALTER TABLE `insee_sirene_legal_unit_history_categorie_entreprise` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_categorie_entreprise_1` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit_history_categorie_entreprise` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_categorie_entreprise_2`;
ALTER TABLE `insee_sirene_legal_unit_history_categorie_entreprise` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_categorie_entreprise_2` FOREIGN KEY (`insee_sirene_categorie_entreprise_id`) REFERENCES `insee_sirene_categorie_entreprise`(`insee_sirene_categorie_entreprise_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_catjur_n3` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_catjur_n3_1`;
ALTER TABLE `insee_sirene_legal_unit_history_catjur_n3` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_catjur_n3_1` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit_history_catjur_n3` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_catjur_n3_2`;
ALTER TABLE `insee_sirene_legal_unit_history_catjur_n3` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_catjur_n3_2` FOREIGN KEY (`insee_catjur_n3_id`) REFERENCES `insee_catjur`.`insee_catjur_n3`(`insee_catjur_n3_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_denomination` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_denomination`;
ALTER TABLE `insee_sirene_legal_unit_history_denomination` ADD CONSTRAINT `fk_insee_sirene_legal_unit_denomination` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_employer` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_employer`;
ALTER TABLE `insee_sirene_legal_unit_history_employer` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_employer` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_etat_administratif` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_etat_administratif_1`;
ALTER TABLE `insee_sirene_legal_unit_history_etat_administratif` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_etat_administratif_1` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit_history_etat_administratif` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_etat_administratif_2`;
ALTER TABLE `insee_sirene_legal_unit_history_etat_administratif` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_etat_administratif_2` FOREIGN KEY (`insee_sirene_etat_administratif_id`) REFERENCES `insee_sirene_etat_administratif`(`insee_sirene_etat_administratif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_naf2008_lv5_subclass` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_naf2008_lv5_subclass_1`;
ALTER TABLE `insee_sirene_legal_unit_history_naf2008_lv5_subclass` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_naf2008_lv5_subclass_1` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit_history_naf2008_lv5_subclass` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_naf2008_lv5_subclass_2`;
ALTER TABLE `insee_sirene_legal_unit_history_naf2008_lv5_subclass` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_naf2008_lv5_subclass_2` FOREIGN KEY (`insee_naf2008_lv5_subclass_id`) REFERENCES `insee_naf`.`insee_naf2008_lv5_subclass`(`insee_naf2008_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_social_economy` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_social_economy`;
ALTER TABLE `insee_sirene_legal_unit_history_social_economy` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_social_economy` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_societe_mission` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_societe_mission_legal_unit`;
ALTER TABLE `insee_sirene_legal_unit_history_societe_mission` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_societe_mission_legal_unit` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_legal_unit_history_tranche_effectif` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_tranche_effectif_1`;
ALTER TABLE `insee_sirene_legal_unit_history_tranche_effectif` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_tranche_effectif_1` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_legal_unit_history_tranche_effectif` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_legal_unit_history_tranche_effectif_2`;
ALTER TABLE `insee_sirene_legal_unit_history_tranche_effectif` ADD CONSTRAINT `fk_insee_sirene_legal_unit_history_tranche_effectif_2` FOREIGN KEY (`insee_sirene_tranche_effectif_id`) REFERENCES `insee_sirene_tranche_effectif`(`insee_sirene_tranche_effectif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_moral_person` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_moral_person_legal_unit`;
ALTER TABLE `insee_sirene_moral_person` ADD CONSTRAINT `fk_insee_sirene_moral_person_legal_unit` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_moral_person_history_raison_sociale` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_moral_person_history_raison_sociale_moral_person`;
ALTER TABLE `insee_sirene_moral_person_history_raison_sociale` ADD CONSTRAINT `fk_insee_sirene_moral_person_history_raison_sociale_moral_person` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_moral_person`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_moral_person_history_sigle` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_moral_person_history_sigle_moral_person`;
ALTER TABLE `insee_sirene_moral_person_history_sigle` ADD CONSTRAINT `fk_insee_sirene_moral_person_history_sigle_moral_person` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_moral_person`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_physical_person` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_physical_person_legal_unit`;
ALTER TABLE `insee_sirene_physical_person` ADD CONSTRAINT `fk_insee_sirene_physical_person_legal_unit` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_physical_person_history` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_physical_person_history_physical_person`;
ALTER TABLE `insee_sirene_physical_person_history` ADD CONSTRAINT `fk_insee_sirene_physical_person_history_physical_person` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_physical_person`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
