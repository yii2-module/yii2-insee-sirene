/**
 * Database relations required by InseeSireneModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-sirene
 * @license MIT
 */

ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_legal_unit`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_legal_unit` FOREIGN KEY (`insee_sirene_legal_unit_id`) REFERENCES `insee_sirene_legal_unit`(`insee_sirene_legal_unit_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_insee_cog_pays`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_insee_cog_pays` FOREIGN KEY (`insee_cog_pays_id`) REFERENCES `insee_cog`.`insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_primary_address`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_primary_address` FOREIGN KEY (`insee_sirene_primary_address_id`) REFERENCES `insee_sirene_establishment_address`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_secondary_address`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_secondary_address` FOREIGN KEY (`insee_sirene_secondary_address_id`) REFERENCES `insee_sirene_establishment_address`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_etat_administratif`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_etat_administratif` FOREIGN KEY (`insee_sirene_etat_administratif_id`) REFERENCES `insee_sirene_etat_administratif`(`insee_sirene_etat_administratif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_tranche_effectif`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_tranche_effectif` FOREIGN KEY (`insee_sirene_tranche_effectif_id`) REFERENCES `insee_sirene_tranche_effectif`(`insee_sirene_tranche_effectif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_naf2008_lv5_subclass`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_naf2008_lv5_subclass` FOREIGN KEY (`insee_naf2008_lv5_subclass_id`) REFERENCES `insee_naf`.`insee_naf2008_lv5_subclass`(`insee_naf2008_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_naf2008_lv6_artisanat`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_naf2008_lv6_artisanat` FOREIGN KEY (`insee_naf2008_lv6_artisanat_id`) REFERENCES `insee_naf`.`insee_naf2008_lv6_artisanat`(`insee_naf2008_lv6_artisanat_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_ban_address`;
ALTER TABLE `insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_ban_address` FOREIGN KEY (`insee_ban_address_id`) REFERENCES `insee_ban`.`insee_ban_address`(`insee_ban_address_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_address` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_address_insee_ban_address`;
ALTER TABLE `insee_sirene_establishment_address` ADD CONSTRAINT `fk_insee_sirene_establishment_address_insee_ban_address` FOREIGN KEY (`insee_ban_address_id`) REFERENCES `insee_ban`.`insee_ban_address`(`insee_ban_address_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_denomination` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_history_denomination`;
ALTER TABLE `insee_sirene_establishment_history_denomination` ADD CONSTRAINT `fk_insee_sirene_establishment_history_denomination` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_employer` DROP CONSTRAINT IF EXISTS `fk_insee_establishment_history_employer`;
ALTER TABLE `insee_sirene_establishment_history_employer` ADD CONSTRAINT `fk_insee_establishment_history_employer` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_enseigne` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_history_enseigne`;
ALTER TABLE `insee_sirene_establishment_history_enseigne` ADD CONSTRAINT `fk_insee_sirene_establishment_history_enseigne` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_etat_administratif` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_history_etat_administratif`;
ALTER TABLE `insee_sirene_establishment_history_etat_administratif` ADD CONSTRAINT `fk_insee_sirene_establishment_history_etat_administratif` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment_history_etat_administratif` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_etat_administratif_establishment_history`;
ALTER TABLE `insee_sirene_establishment_history_etat_administratif` ADD CONSTRAINT `fk_insee_sirene_etat_administratif_establishment_history` FOREIGN KEY (`insee_sirene_etat_administratif_id`) REFERENCES `insee_sirene_etat_administratif`(`insee_sirene_etat_administratif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_naf2008_lv5_subclass` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_history_naf2008_lv5_subclass`;
ALTER TABLE `insee_sirene_establishment_history_naf2008_lv5_subclass` ADD CONSTRAINT `fk_insee_sirene_establishment_history_naf2008_lv5_subclass` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment_history_naf2008_lv5_subclass` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_naf2008_lv1_subclass_establishment_history`;
ALTER TABLE `insee_sirene_establishment_history_naf2008_lv5_subclass` ADD CONSTRAINT `fk_insee_sirene_naf2008_lv1_subclass_establishment_history` FOREIGN KEY (`insee_naf2008_lv5_subclass_id`) REFERENCES `insee_naf`.`insee_naf2008_lv5_subclass`(`insee_naf2008_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_naf2008_lv6_artisanat` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_history_naf2008_lv6_artisanat`;
ALTER TABLE `insee_sirene_establishment_history_naf2008_lv6_artisanat` ADD CONSTRAINT `fk_insee_sirene_establishment_history_naf2008_lv6_artisanat` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment_history_naf2008_lv6_artisanat` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_naf2008_lv6_artisanat_establishment_history`;
ALTER TABLE `insee_sirene_establishment_history_naf2008_lv6_artisanat` ADD CONSTRAINT `fk_insee_sirene_naf2008_lv6_artisanat_establishment_history` FOREIGN KEY (`insee_naf2008_lv6_artisanat_id`) REFERENCES `insee_naf`.`insee_naf2008_lv6_artisanat`(`insee_naf2008_lv6_artisanat_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_siege` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_history_siege`;
ALTER TABLE `insee_sirene_establishment_history_siege` ADD CONSTRAINT `fk_insee_sirene_establishment_history_siege` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_establishment_history_tranche_effectif` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_establishment_history_tranche_effectif`;
ALTER TABLE `insee_sirene_establishment_history_tranche_effectif` ADD CONSTRAINT `fk_insee_sirene_establishment_history_tranche_effectif` FOREIGN KEY (`insee_sirene_establishment_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_establishment_history_tranche_effectif` DROP CONSTRAINT IF EXISTS `fk_insee_tranche_effectif_establishment_history`;
ALTER TABLE `insee_sirene_establishment_history_tranche_effectif` ADD CONSTRAINT `fk_insee_tranche_effectif_establishment_history` FOREIGN KEY (`insee_sirene_tranche_effectif_id`) REFERENCES `insee_sirene_tranche_effectif`(`insee_sirene_tranche_effectif_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_sirene_succession` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_succession_next_establishment`;
ALTER TABLE `insee_sirene_succession` ADD CONSTRAINT `fk_insee_sirene_succession_next_establishment` FOREIGN KEY (`insee_sirene_establishment_next_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_sirene_succession` DROP CONSTRAINT IF EXISTS `fk_insee_sirene_succession_prev_establishment`;
ALTER TABLE `insee_sirene_succession` ADD CONSTRAINT `fk_insee_sirene_succession_prev_establishment` FOREIGN KEY (`insee_sirene_establishment_prev_id`) REFERENCES `insee_sirene_establishment`(`insee_sirene_establishment_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
