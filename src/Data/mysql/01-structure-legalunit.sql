/**
 * Database structure required by InseeSireneModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-sirene
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `insee_sirene_metadata`;
DROP TABLE IF EXISTS `insee_sirene_categorie_entreprise`;
DROP TABLE IF EXISTS `insee_sirene_etat_administratif`;
DROP TABLE IF EXISTS `insee_sirene_tranche_effectif`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_categorie_entreprise`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_etat_administratif`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_tranche_effectif`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_catjur_n3`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_naf2008_lv5_subclass`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_denomination`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_social_economy`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_employer`;
DROP TABLE IF EXISTS `insee_sirene_legal_unit_history_societe_mission`;
DROP TABLE IF EXISTS `insee_sirene_moral_person`;
DROP TABLE IF EXISTS `insee_sirene_moral_person_history_sigle`;
DROP TABLE IF EXISTS `insee_sirene_moral_person_history_raison_sociale`;
DROP TABLE IF EXISTS `insee_sirene_physical_person`;
DROP TABLE IF EXISTS `insee_sirene_physical_person_history`;

SET foreign_key_checks = 1;


CREATE TABLE `insee_sirene_metadata`
(
	`insee_sirene_metadata_id` VARCHAR(64) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The key',
	`contents` VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The value'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene metadata';

CREATE TABLE `insee_sirene_categorie_entreprise`
(
	`insee_sirene_categorie_entreprise_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the categorie entreprise',
	`code` VARCHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the categorie entreprise',
	`description` VARCHAR(127) NOT NULL COLLATE utf8_general_ci COMMENT 'The description of the categorie entreprise'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene categorie entreprise';

CREATE TABLE `insee_sirene_etat_administratif`
(
	`insee_sirene_etat_administratif_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the etat administratif',
	`code` CHAR(1) NOT NULL COLLATE ascii_bin COMMENT 'The id of the etat administratif',
	`description` VARCHAR(127) NOT NULL COLLATE utf8_general_ci COMMENT 'The description of the etat administratif'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene etat administratif';

CREATE TABLE `insee_sirene_tranche_effectif`
(
	`insee_sirene_tranche_effectif_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the tranche effectif',
	`code` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The code of the tranche effectif',
	`description` VARCHAR(127) NOT NULL COLLATE utf8_general_ci COMMENT 'The description of the tranche effectif',
	`min` INT(11) NOT NULL COMMENT 'The min number in the tranche effectif',
	`max` INT(11) NOT NULL COMMENT 'The max number in the tranche effectif'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene tranche effectif';

CREATE TABLE `insee_sirene_legal_unit`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The siren number of this legal unit',
	`date_creation` DATE DEFAULT NULL COMMENT 'The date of creation of this legal unit',
	`date_last_treatment` DATE DEFAULT NULL COMMENT 'The date of last treatment of this legal unit',
	`insee_rna_id` CHAR(10) DEFAULT NULL COLLATE ascii_bin COMMENT 'The related id of the RNA',
	`insee_sirene_category_entreprise_id` INT(11) DEFAULT NULL COMMENT 'The id of the related categorie entreprise',
	`date_since_category_entreprise` DATE DEFAULT NULL COMMENT 'The date when the period for the categorie entreprise started',
	`insee_sirene_etat_administratif_id` INT(11) DEFAULT NULL COMMENT 'The id of the related etat administratif',
	`date_since_etat_administratif` DATE DEFAULT NULL COMMENT 'The date when the period for the etat administratif started',
	`insee_sirene_tranche_effectif_id` INT(11) DEFAULT NULL COMMENT 'The id of the related tranche effectif',
	`date_since_tranche_effectif` DATE DEFAULT NULL COMMENT 'The date when the period for the tranche effectif started',
	`insee_catjur_n3_id` INT(11) DEFAULT NULL COMMENT 'The id of the related juridic category lv3',
	`date_since_catjur_n3` DATE DEFAULT NULL COMMENT 'The date when the period for the juridic category lv3',
	`insee_naf2008_lv5_subclass_id` CHAR(6) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 subclass',
	`date_since_naf2008_lv5_subclass` DATE DEFAULT NULL COMMENT 'The date when the period for the related naf2 subclass',
	`usage_denomination_1` VARCHAR(70) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The first usage denomination of this legal unit',
	`usage_denomination_2` VARCHAR(70) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The second usage denomination of this legal unit',
	`usage_denomination_3` VARCHAR(70) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The third usage denomination of this legal unit',
	`date_since_denomination` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`is_social_economy` BOOLEAN DEFAULT FALSE COMMENT 'Whether this legal unit is in the field of social economy',
	`date_since_social_economy` DATE DEFAULT NULL COMMENT 'The date when the is social economy started',
	`is_employer` BOOLEAN DEFAULT FALSE COMMENT 'Whether this legal unit is employer',
	`date_since_employer` DATE DEFAULT NULL COMMENT 'The date when the is employer started',
	`is_societe_mission` BOOLEAN DEFAULT FALSE COMMENT 'Whether this legal unit is a societe a mission',
	`date_since_societe_mission` DATE DEFAULT NULL COMMENT 'The date when the is societe mission started'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units';

CREATE TABLE `insee_sirene_legal_unit_history_categorie_entreprise`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of the related legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_sirene_categorie_entreprise_id` INT(11) NOT NULL COMMENT 'The id of the related categorie entreprise',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units histories for categorie entreprise';

CREATE TABLE `insee_sirene_legal_unit_history_etat_administratif`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of the related legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_sirene_etat_administratif_id` INT(11) NOT NULL COMMENT 'The id of the related etat administratif',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units histories for etat administratif';

CREATE TABLE `insee_sirene_legal_unit_history_tranche_effectif`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of the related legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_sirene_tranche_effectif_id` INT(11) NOT NULL COMMENT 'The id of the related tranche effectif',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units histories for tranches effectifs';

CREATE TABLE `insee_sirene_legal_unit_history_catjur_n3`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of the related legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_catjur_n3_id` INT(11) NOT NULL COMMENT 'The id of the related juridic category lv3',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units histories for juridic categories';

CREATE TABLE `insee_sirene_legal_unit_history_naf2008_lv5_subclass`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of the related legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_naf2008_lv5_subclass_id` CHAR(6) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 subclass',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units histories for naf classes';

CREATE TABLE `insee_sirene_legal_unit_history_denomination`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of the related legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`usage_denomination_1` VARCHAR(70) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The first usage denomination of this legal unit',
	`usage_denomination_2` VARCHAR(70) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The second usage denomination of this legal unit',
	`usage_denomination_3` VARCHAR(70) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The third usage denomination of this legal unit',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units histories for denominations';

CREATE TABLE `insee_sirene_legal_unit_history_social_economy`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of the related legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`is_social_economy` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Whether this legal unit is in the field of social economy',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal units histories for social economy status';

CREATE TABLE `insee_sirene_legal_unit_history_employer`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of this legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`is_employer` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Whether this legal unit is employer',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal unit employer historys';

CREATE TABLE `insee_sirene_legal_unit_history_societe_mission`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of this legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`is_societe_mission` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Whether this legal unit is employer',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene legal unit societe mission historys';

CREATE TABLE `insee_sirene_moral_person`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of this legal unit',
	`sigle` VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The sigle of this legal unit',
	`date_since_sigle` DATE DEFAULT NULL COMMENT 'The date when the period for the sigle started',
	`raison_sociale` VARCHAR(120) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The denomination / raison sociale of this legal unit',
	`date_since_raison_sociale` DATE DEFAULT NULL COMMENT 'The date when the period for the raison sociale started'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene moral persons';

CREATE TABLE `insee_sirene_moral_person_history_sigle`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The id of this legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`sigle` VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The sigle of this legal unit',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene moral person history for sigles';

CREATE TABLE `insee_sirene_moral_person_history_raison_sociale`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The siren number of this legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`raison_sociale` VARCHAR(120) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The denomination / raison sociale of this legal unit',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene moral person historys';

CREATE TABLE `insee_sirene_physical_person`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The siren number of this legal unit',
	`gender` CHAR(1) DEFAULT NULL COLLATE ascii_bin COMMENT 'The gender of the physical person, M = man, F = woman',
	`prenom_1` VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The first first name of this physical person',
	`prenom_2` VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The second first name of this physical person',
	`prenom_3` VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The third first name of this physical person',
	`prenom_4` VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The fourth first name of this physical person',
	`usage_prenom` VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The usage first name of this physical person',
	`pseudonym` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The pseudo of this physical person',
	`date_since_name` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`name` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The last name of this physical person',
	`usage_name` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The usage last name of this physical person'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene physical persons';

CREATE TABLE `insee_sirene_physical_person_history`
(
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The siren number of this legal unit',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`name` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The last name of this physical person',
	`usage_name` VARCHAR(100) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The usage last name of this physical person',
	PRIMARY KEY (`insee_sirene_legal_unit_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene physical person historys';
