/**
 * Database structure required by InseeSireneModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-sirene
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `insee_sirene_establishment`;
DROP TABLE IF EXISTS `insee_sirene_establishment_address`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_etat_administratif`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_enseigne`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_denomination`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_naf2008_lv5_subclass`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_naf2008_lv6_artisanat`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_employer`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_siege`;
DROP TABLE IF EXISTS `insee_sirene_establishment_history_tranche_effectif`;
DROP TABLE IF EXISTS `insee_sirene_succession`;

SET foreign_key_checks = 1;

CREATE TABLE `insee_sirene_establishment`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL PRIMARY KEY COMMENT 'The siret number of this establishment',
	`insee_sirene_legal_unit_id` INT(11) NOT NULL COMMENT 'The siren number of this legal unit',
	`date_creation` DATE DEFAULT NULL COMMENT 'The date of creation of this establishment',
	`date_last_treatment` DATE DEFAULT NULL COMMENT 'The date of last treatment of this legal unit',
	`insee_cog_pays_id` CHAR(7) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related cog country',
	`insee_sirene_primary_address_id` BIGINT(14) DEFAULT NULL COMMENT 'The id of the related primary address',
	`insee_sirene_secondary_address_id` BIGINT(14) DEFAULT NULL COMMENT 'The id of the related secondary address',
	`date_since_etat_administratif` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`insee_sirene_etat_administratif_id` INT(11) DEFAULT NULL COMMENT 'The administrative state of this establishment',
	`date_since_tranche_effectif` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`insee_sirene_tranche_effectif_id` INT(11) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the tranche effectif',
	`date_since_naf2008_lv5_subclass` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`insee_naf2008_lv5_subclass_id` CHAR(6) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 subclass',
	`date_since_naf2008_lv6_artisanat` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`insee_naf2008_lv6_artisanat_id` CHAR(8) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 artisanat',
	`date_since_enseigne` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`enseigne_1` VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The first name of this establishment',
	`enseigne_2` VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The second name of this establishment',
	`enseigne_3` VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The third name of this establishment',
	`date_since_usage_denomination` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`usage_denomination` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The usage denomination of this establishment',
	`date_since_is_employer` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`is_employer` BOOLEAN DEFAULT NULL COMMENT 'Whether this legal unit is employer',
	`date_since_is_siege` DATE DEFAULT NULL COMMENT 'The date when this period started',
	`is_siege` BOOLEAN DEFAULT NULL COMMENT 'Whether this establishment is the siege of the legal unit',
	`insee_ban_address_id` CHAR(22) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the related ban address',
	`insee_ban_address_fitness` SMALLINT(3) NOT NULL DEFAULT 0 COMMENT 'The fitness index of the related ban address (0 = nul, 255 = max)',
	`insee_ban_address_raw` VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The raw data of the ban address',
	`complement_address` VARCHAR(38) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The complement of the address',
	`libelle_foreign_city` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the foreign city if the address is not french',
	`special_distribution` VARCHAR(26) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The special distribution for the address',
	`cedex_code` VARCHAR(9) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The code of the cedex, if any',
	`cedex_label` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The label of the cedex, if any'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishments';

CREATE TABLE `insee_sirene_establishment_address`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL PRIMARY KEY COMMENT 'The siret number of the establishment at this address (+/-1)',
	`insee_ban_address_id` CHAR(22) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the related ban address',
	`insee_ban_address_fitness` SMALLINT(3) NOT NULL DEFAULT 0 COMMENT 'The fitness index of the related ban address (0 = nul, 255 = max)',
	`insee_ban_address_raw` VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The raw data of the ban address',
	`complement_address` VARCHAR(38) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The complement of the address',
	`libelle_foreign_city` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the foreign city if the address is not french',
	`special_distribution` VARCHAR(26) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The special distribution for the address',
	`cedex_code` VARCHAR(9) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The code of the cedex, if any',
	`cedex_label` VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The label of the cedex, if any'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene addresses';

CREATE TABLE `insee_sirene_establishment_history_etat_administratif`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_sirene_etat_administratif_id` INT(11) NOT NULL COMMENT 'The administrative state of this establishment',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment history with etat administratif';

CREATE TABLE `insee_sirene_establishment_history_enseigne`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`enseigne_1` VARCHAR(50) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The first name of this establishment',
	`enseigne_2` VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The second name of this establishment',
	`enseigne_3` VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The third name of this establishment',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment history with enseignes';

CREATE TABLE `insee_sirene_establishment_history_denomination`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`usage_denomination` VARCHAR(100) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The usage denomination of this establishment',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment history with denomination';

CREATE TABLE `insee_sirene_establishment_history_naf2008_lv5_subclass`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_naf2008_lv5_subclass_id` CHAR(6) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 subclass',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment history with activite principale';

CREATE TABLE `insee_sirene_establishment_history_naf2008_lv6_artisanat`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_naf2008_lv6_artisanat_id` CHAR(8) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 artisanat',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment history with activite metier';

CREATE TABLE `insee_sirene_establishment_history_employer`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`is_employer` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Whether this legal unit is employer',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment history with caractere employeur';

CREATE TABLE `insee_sirene_establishment_history_siege`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`is_siege` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Whether this establishment is the siege of the legal unit',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment history with siege';

CREATE TABLE `insee_sirene_establishment_history_tranche_effectif`
(
	`insee_sirene_establishment_id` BIGINT(14) NOT NULL COMMENT 'The siret number of this establishment',
	`date_since` DATE NOT NULL COMMENT 'The date when this period started',
	`insee_sirene_tranche_effectif_id` INT(11) NOT NULL COLLATE ascii_bin COMMENT 'The id of the tranche effectif',
	PRIMARY KEY (`insee_sirene_establishment_id`, `date_since`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment historys';

CREATE TABLE `insee_sirene_succession`
(
	`insee_sirene_establishment_prev_id` BIGINT(14) NOT NULL COLLATE ascii_bin COMMENT 'The siret number of the prev establishment',
	`insee_sirene_establishment_next_id` BIGINT(14) NOT NULL COLLATE ascii_bin COMMENT 'The siret number of the next establishment',
	`date_dependancy` DATE NOT NULL COMMENT 'The date when the dependancy took place',
	`date_last_treatment` DATE NOT NULL COMMENT 'The date of last treatment of this legal unit',
	`siege_transfer` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Whether the siege was transferred',
	`economic_continuity` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Whether economic continuity was performed',
	PRIMARY KEY (`insee_sirene_establishment_prev_id`, `insee_sirene_establishment_next_id`, `date_dependancy`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene establishment dependancies';
