<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Commands;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpoint;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\Reifier\ReificationThrowable;
use RuntimeException;
use yii\BaseYii;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneLegalUnitHistoryUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneLegalUnitUpdater;
use Yii2Module\Yii2InseeSirene\InseeSireneModule;

/**
 * ActiviteController class file.
 * 
 * This command checks the validity of the files regarding the activites and
 * nomenclatures that are present in the files.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ActiviteController extends ExtendedController
{
	
	/**
	 * Checks for all the legal units.
	 *
	 * @return integer the error code, 0 if no error
	 */
	public function actionLegalUnit() : int
	{
		return $this->runCallable(function() : int
		{
			$updater = new InseeSireneLegalUnitUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->checkNomenclatureActivite($endpoint);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Checks for all the legal unit histories.
	 * 
	 * @return int
	 */
	public function actionLegalUnitHistory() : int
	{
		return $this->runCallable(function() : int
		{
			$updater = new InseeSireneLegalUnitHistoryUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->checkNomenclatureActivite($endpoint);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Gets the sirene endpoint with its dependancies.
	 *
	 * @return ApiFrInseeSireneEndpointInterface
	 * @throws \yii\base\InvalidArgumentException
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	protected function getSireneEndpoint() : ApiFrInseeSireneEndpointInterface
	{
		$tempDirPath = (string) BaseYii::getAlias('@app/runtime/'.InseeSireneModule::getInstance()->id);
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($this->getLogger());
		$clientFactory->getConfiguration()->disablePreferCurl();
		$client = $clientFactory->createClient();
		
		return new ApiFrInseeSireneEndpoint($tempDirPath, $client);
	}
	
}
