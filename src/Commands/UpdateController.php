<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-sirene library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeSirene\Commands;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpoint;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\Reifier\ReificationThrowable;
use RuntimeException;
use yii\BaseYii;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneCategorieEntrepriseUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneEstablishmentHistoryUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneEstablishmentUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneEtatAdministratifUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneLegalUnitHistoryUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneLegalUnitUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneSuccessionUpdater;
use Yii2Module\Yii2InseeSirene\Components\InseeSireneTrancheEffectifUpdater;
use Yii2Module\Yii2InseeSirene\InseeSireneModule;

/**
 * UpdateController class file.
 * 
 * This commands updates all parts of the insee sirene database.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class UpdateController extends ExtendedController
{
	
	/**
	 * Updates all the classes of the sirene database.
	 * 
	 * @return integer the error code, 0 if no error
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function actionAll() : int
	{
		/** @psalm-suppress PossiblyFalseArgument */
		$stopAt = (new DateTimeImmutable())->add(DateInterval::createFromDateString('+94 hours')); // 4 days minus 2 hours
		
		$ret = $this->actionCategorieEntreprise($stopAt);
		if(ExitCode::OK !== $ret || \time() > $stopAt->getTimestamp())
		{
			return $ret;
		}
		
		$ret = $this->actionEtatAdministratif($stopAt);
		if(ExitCode::OK !== $ret || \time() > $stopAt->getTimestamp())
		{
			return $ret;
		}
		
		$ret = $this->actionTrancheEffectif($stopAt);
		if(ExitCode::OK !== $ret || \time() > $stopAt->getTimestamp())
		{
			return $ret;
		}
		
		$ret = $this->actionLegalUnit($stopAt);
		if(ExitCode::OK !== $ret || \time() > $stopAt->getTimestamp())
		{
			return $ret;
		}
		
		$ret = $this->actionEstablishment($stopAt);
		if(ExitCode::OK !== $ret || \time() > $stopAt->getTimestamp())
		{
			return $ret;
		}
		
		$ret = $this->actionSuccession($stopAt);
		if(ExitCode::OK !== $ret || \time() > $stopAt->getTimestamp())
		{
			return $ret;
		}
		
		$ret = $this->actionLegalUnitHistory($stopAt);
		if(ExitCode::OK !== $ret || \time() > $stopAt->getTimestamp())
		{
			return $ret;
		}
		
		return $this->actionEstablishmentHistory($stopAt);
	}
	
	/**
	 * Updates the categories entreprises from the current file given by the insee.
	 *
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionCategorieEntreprise(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneCategorieEntrepriseUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the etats administratifs from the current file given by the insee.
	 *
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionEtatAdministratif(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneEtatAdministratifUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the tranches effectifs from the current file given by the insee.
	 *
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionTrancheEffectif(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneTrancheEffectifUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the legal units from the current file given by the insee.
	 * 
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionLegalUnit(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneLegalUnitUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the legal unit histories from the current file given by the
	 * insee.
	 * 
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionLegalUnitHistory(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneLegalUnitHistoryUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the establishment from the current file given by the insee.
	 * 
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionEstablishment(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneEstablishmentUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the establishment histories from the current file given by the
	 * insee.
	 * 
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionEstablishmentHistory(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneEstablishmentHistoryUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the establishment dependancies from the current file given by
	 * the insee.
	 * 
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionSuccession(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeSireneSuccessionUpdater($this->getLogger());
			$endpoint = $this->getSireneEndpoint();
			$updater->updateAll($endpoint, $stopAt, $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Gets the sirene endpoint with its dependancies.
	 * 
	 * @return ApiFrInseeSireneEndpointInterface
	 * @throws \yii\base\InvalidArgumentException
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	protected function getSireneEndpoint() : ApiFrInseeSireneEndpointInterface
	{
		$tempDirPath = (string) BaseYii::getAlias('@app/runtime/'.InseeSireneModule::getInstance()->id);
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($this->getLogger());
		$clientFactory->getConfiguration()->disablePreferCurl();
		$client = $clientFactory->createClient();
		
		return new ApiFrInseeSireneEndpoint($tempDirPath, $client);
	}
	
}
